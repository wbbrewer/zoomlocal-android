package com.zoomlocal.listings.util;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Locale;
import java.util.regex.Pattern;

import okhttp3.ResponseBody;

/**
 * Created by root on 3/11/15.
 */
public class Util {
    public static final String EMAIL_REG_EXP = "(?:[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    public static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REG_EXP);

    private static final double METERS_IN_MILE = 0.00062137;

    public static final String USERNAME_PATTERN = "^[a-zA-Z0-9._]{1,16}$";

    public static boolean isEmailValid(String email) {
        if (email != null) {
            email = email.toLowerCase(Locale.US).trim();
            return EMAIL_PATTERN.matcher(email).matches();
        } else {
            return false;
        }
    }

    public static String safetrim(String string) {
        if (string == null) {
            return null;
        }
        int length = string.length();
        if (length == 0) {
            return string;
        }
        int start = 0;
        while (start < length && Character.isWhitespace(string.charAt(start))) {
            start++;
        }
        int end = length;
        while (end > start && Character.isWhitespace(string.charAt(end - 1))) {
            end--;
        }
        if (start >= end) {
            return "";
        }
        return string.substring(start, end);
    }


    public static boolean isNullOrEmpty(String argument) {
        return argument == null || safetrim(argument).length() == 0;
    }

    public static LatLng toLatLng(Location location) {
        return new LatLng(location.getLatitude(), location.getLongitude());
    }

    public static double metersToMiles(double meters) {
        return meters * METERS_IN_MILE;
    }

    public static double milesToMeters(double miles) {
        return miles / METERS_IN_MILE;
    }

    public static boolean locationPermissionGranted(Context context) {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
            || Build.VERSION.SDK_INT < Build.VERSION_CODES.M;
    }

    public static boolean writeResponseBodyToDisk(File file, ResponseBody body) {
        try {
            InputStream inputStream = null;
            OutputStream outputStream = null;
            try {
                byte[] fileReader = new byte[4096];
                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(file);

                while (true) {
                    int read = inputStream.read(fileReader);
                    if (read == -1) {
                        break;
                    }
                    outputStream.write(fileReader, 0, read);
                    fileSizeDownloaded += read;
                    Log.d("test", "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }

                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }
}
