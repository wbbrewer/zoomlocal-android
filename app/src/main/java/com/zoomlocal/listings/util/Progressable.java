package com.zoomlocal.listings.util;

/**
 */
public interface Progressable {
    void updateProgress(boolean isVisible);
}
