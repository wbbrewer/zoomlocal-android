package com.zoomlocal.listings.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.WindowManager;

import java.io.Closeable;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 *
 */
public class UiUtils {

    public static DisplayMetrics getDisplayMetrics(Context context) {
        DisplayMetrics metrics = new DisplayMetrics();
        ((WindowManager) context.getSystemService(Activity.WINDOW_SERVICE)).getDefaultDisplay().getMetrics(metrics);
        return metrics;
    }

    public static int dpToPx(Resources resources, int dp) {
        DisplayMetrics d = resources.getDisplayMetrics();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, d);
    }

    /**
     * Describes predicate
     */
    public interface Predicate<T> {
        boolean evaluate(T object);
    }

    public static void setVisibility(boolean visible, View...views) {
        for (View view : views) {
            view.setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }

    /**
     * Filter specified list.
     *
     * <p>
     * Creates new list and fill its with items from sourceList which are evaluated to true by specified predicate.
     * </p>
     *
     * @param sourceList List source list.
     * @param predicate Predicate a predicate to use to evaluate list object.
     * @return List filtered list.
     */
    public static <T> List<T> filter(List<T> sourceList, Predicate<T> predicate) {
        List<T> filteredList = new LinkedList<T>();
        for (T object : sourceList) {
            if (predicate.evaluate(object)) {
                filteredList.add(object);
            }
        }
        return filteredList;
    }



    public static void closeQuietly(Closeable closeable) {
        try {
            if (closeable != null) {
                closeable.close();
            }
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
