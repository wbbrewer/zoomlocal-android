package com.zoomlocal.listings.util;

import android.view.View;

/**
 *
 */

public class ViewProgressable implements Progressable {
    private View view;

    public ViewProgressable(View view) {
        this.view = view;
    }

    @Override
    public void updateProgress(boolean isVisible) {
        UiUtils.setVisibility(isVisible, view);
    }
}
