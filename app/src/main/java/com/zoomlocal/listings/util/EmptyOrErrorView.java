package com.zoomlocal.listings.util;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.zoomlocal.listings.R;

import java.net.UnknownHostException;

import retrofit2.Response;

/**
 *
 */
//todo: to view model
public class EmptyOrErrorView extends FrameLayout {
    private ImageView iconImage;
    private TextView titleText;
    private TextView descriptionText;

    public EmptyOrErrorView(Context context) {
        super(context);
        init();
    }

    public EmptyOrErrorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public EmptyOrErrorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_empty_list, this);

        iconImage = (ImageView) findViewById(R.id.iconImage);
        titleText = (TextView) findViewById(R.id.titleText);
        descriptionText = (TextView) findViewById(R.id.descriptionText);

        hide();
    }

    public void hide() {
        UiUtils.setVisibility(false, this);
    }

    public void showCustom(String title, String description) {
        UiUtils.setVisibility(false, iconImage);
        UiUtils.setVisibility(true, this);

        titleText.setText(title);
        descriptionText.setText(description);
    }

    public void showEmptyView(boolean showEmptyView) {
        UiUtils.setVisibility(false, iconImage);
        UiUtils.setVisibility(showEmptyView, this);

        titleText.setText("No Results");
        descriptionText.setText("Sorry, we could not find any items.");
    }

    public void showNetworkError(Throwable t) {
        UiUtils.setVisibility(true, this);
        UiUtils.setVisibility(true, iconImage);
        titleText.setText("Error");

        String text = t.getMessage();
        if (t instanceof UnknownHostException) {
            text = "No Internet connection is available.";
        }
        descriptionText.setText(text);
    }

    public void showServerError(Response response) {
        UiUtils.setVisibility(true, this);
        UiUtils.setVisibility(true, iconImage);

        titleText.setText("Error");

        //todo: unique handler with DefaultCallback
        descriptionText.setText(TextUtils.isEmpty(response.message()) ? "Unknown server error" : "Request failed: " + response.message());
    }
}
