package com.zoomlocal.listings.util;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by yklishevich on 26/01/17.
 */

public class AppPushBroadcastReceiver extends ParsePushBroadcastReceiver {

    private static final String TAG = "AppPushBroadcastReceiver";

    @Override
    protected void onPushReceive(Context context, Intent intent) {

        try {
            JSONObject pushData = new JSONObject(intent.getStringExtra("com.parse.Data"));

            Log.v(TAG, "Push received!");

            String action;
            action = pushData.optString("action", null);

            if (action != null) {
                Bundle notification = intent.getExtras();
                Intent broadcastIntent = new Intent();
                broadcastIntent.putExtras(notification);
                broadcastIntent.setAction(action);
                broadcastIntent.setPackage(context.getPackageName());
                context.sendBroadcast(broadcastIntent);
            }

        } catch (JSONException var7) {
            Log.e("ParsePushReceiver", "Unexpected JSONException when receiving push data: ", var7);
        }
    }

}
