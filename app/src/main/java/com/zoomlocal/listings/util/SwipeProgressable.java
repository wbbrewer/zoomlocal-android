package com.zoomlocal.listings.util;

import android.support.v4.widget.SwipeRefreshLayout;

/**
 *
 */

public class SwipeProgressable implements Progressable {
    private SwipeRefreshLayout swipeRefresh;

    public SwipeProgressable(SwipeRefreshLayout swipeRefresh) {
        this.swipeRefresh = swipeRefresh;
    }

    @Override
    public void updateProgress(boolean isVisible) {
        if (!isVisible) {
            // swipe refresh is visible automatically after user operation. So we just need to hide it
            swipeRefresh.setRefreshing(false);
        }
    }
}
