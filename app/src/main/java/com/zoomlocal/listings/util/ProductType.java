package com.zoomlocal.listings.util;

/**
 * Created by aricbrown on 1/22/16.
 */
public enum ProductType {
    LISTING,
    EVENT,
    ALL;
}
