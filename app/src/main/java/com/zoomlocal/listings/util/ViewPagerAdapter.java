package com.zoomlocal.listings.util;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public abstract class ViewPagerAdapter<T> extends PagerAdapter {
    private List<T> list;

    private View mCurrentView;

    public ViewPagerAdapter() {
        list = new ArrayList<T>();
    }

    public void setData(T... array) {
        this.list = Arrays.asList(array);
        notifyDataSetChanged();
    }

    public void setData(List<T> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public View getCurrentView() {
        return mCurrentView;
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        mCurrentView = (View) object;
    }

    public T getItem(int position) {
        return list.get(position);
    }

    public abstract View createView(Context context, T item);

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        T item = getItem(position);

        View view = createView(container.getContext(), item);

        container.addView(view, 0);

        return view;
    }

    @Override
    public void destroyItem(View collection, int position, Object view) {
        ((ViewGroup) collection).removeView((View) view);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }

}
