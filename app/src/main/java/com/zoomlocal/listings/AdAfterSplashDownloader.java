package com.zoomlocal.listings;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.zoomlocal.listings.App;
import com.zoomlocal.listings.model.Images;
import com.zoomlocal.listings.model.SizedImage;
import com.zoomlocal.listings.rest.APIService;
import com.zoomlocal.listings.rest.DefaultCallback;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by yklishevich on 02/02/17.
 */

public class AdAfterSplashDownloader {
    private final APIService apiService = App.getInstance().getAPIService();

    /**
     * Bitmap with the advertisement image that is shown after app lauches.
     */
    public Bitmap mAdAfterSplashBitmap;

    private Target mPicassoTarget;

    public interface UpdateCallback {
        void onSuccess();
        void onFailure();
    }

    public void setAdAfterSplashBitmap(Bitmap adAfterSplashBitmap) {
        mAdAfterSplashBitmap = adAfterSplashBitmap;
    }

    public Bitmap getAdAfterSplashBitmap() {
        return mAdAfterSplashBitmap;
    }

    public void updateAdAfterSplashBitmap(final Context context, final UpdateCallback callback) {
        Call<List<Images>> advertisementsCall = apiService.getAdvertisements();
        advertisementsCall.enqueue(new DefaultCallback<List<Images>>(context) {

            @Override
            protected void onSuccess(final Call<List<Images>> call, Response<List<Images>> response) {
                super.onSuccess(call, response);

                List<Images> adImagesList = response.body();
                SizedImage large = adImagesList.get(0).getLarge();

                DisplayMetrics metrics = context.getResources().getDisplayMetrics();
                float scalingFactorForDIP = metrics.density;
                int widthToResize = Math.round(scalingFactorForDIP * large.getWidth());
                int heightToResize = Math.round(scalingFactorForDIP * large.getHeight());

                mPicassoTarget = new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        mAdAfterSplashBitmap = bitmap;
                        callback.onSuccess();
                        mPicassoTarget = null;
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                        mAdAfterSplashBitmap = null;
                        callback.onFailure();
                        mPicassoTarget = null;
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                };

                Picasso.with(context)
                        .load(large.getUrl())
                        .resize(widthToResize, heightToResize)
                        .centerInside()
                        .into(mPicassoTarget);
            }

            @Override
            protected void onServerError(Call<List<Images>> call, Response<List<Images>> response) {
                mAdAfterSplashBitmap = null;
                callback.onFailure();
            }

            @Override
            protected void onNetworkError(Call<List<Images>> call, Throwable t) {
                mAdAfterSplashBitmap = null;
                callback.onFailure();
            }
        });
    }

}
