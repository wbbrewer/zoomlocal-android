package com.zoomlocal.listings.model.deserializers;

import android.util.Log;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.zoomlocal.listings.model.LocationHolder;

import java.lang.reflect.Type;

/**
 */
public class LocationHolderDeserializer implements JsonDeserializer<LocationHolder> {
    private static final String TAG = "LocationHolderDeserial";

    @Override
    public LocationHolder deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        // geoloc=30.4228779,-89.08714359999999
        String geoLocation = json.getAsString();

        try {
            String[] latlong = geoLocation.split(",");
            double latitude = Double.parseDouble(latlong[0]);
            double longitude = Double.parseDouble(latlong[1]);
            return new LocationHolder(latitude, longitude);
        } catch (Exception e) {
            Log.e(TAG, "Cannot parse LocationHolder: " + geoLocation, e);
            return new LocationHolder(0, 0);
        }
    }
}
