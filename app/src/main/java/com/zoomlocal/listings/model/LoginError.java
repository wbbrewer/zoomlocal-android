package com.zoomlocal.listings.model;

import com.google.gson.annotations.SerializedName;

/**
 */
public class LoginError {
    @SerializedName("error")
    private String errorType;

    private String errorDescription;

    public String getErrorType() {
        return errorType;
    }

    public String getErrorDescription() {
        return errorDescription;
    }
}
