package com.zoomlocal.listings.model;

import java.io.Serializable;

public class Category implements Serializable {

    private long termId;
    private String name;
    private String slug;
    private long termGroup;
    private long termTaxonomyId;
    private String taxonomy;
    private String description;
    private long parent;
    private int count;
    private long objectId;
    private String filter;

    public long getTermId() {
        return termId;
    }

    public String getName() {
        return name;
    }

    public String getSlug() {
        return slug;
    }

    public long getTermGroup() {
        return termGroup;
    }

    public long getTermTaxonomyId() {
        return termTaxonomyId;
    }

    public String getTaxonomy() {
        return taxonomy;
    }

    public String getDescription() {
        return description;
    }

    public long getParent() {
        return parent;
    }

    public int getCount() {
        return count;
    }

    public long getObjectId() {
        return objectId;
    }

    public String getFilter() {
        return filter;
    }
}
