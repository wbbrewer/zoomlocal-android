package com.zoomlocal.listings.model;

import java.io.Serializable;

/**
 *
 */

public class OpenClosePair implements Serializable {
    private String openTime;
    private String closeTime;

    public OpenClosePair() {
    }

    public OpenClosePair(String openTime, String closeTime) {
        this.openTime = openTime;
        this.closeTime = closeTime;
    }

    public String getOpenTime() {
        return openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }
}
