package com.zoomlocal.listings.model.deserializers;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.zoomlocal.listings.model.OpenClosePair;

import java.lang.reflect.Type;

/**
 */
public class OpenClosePairDeserializer implements JsonDeserializer<OpenClosePair> {
    @Override
    public OpenClosePair deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonArray jsonArray = json.getAsJsonArray();

        return new OpenClosePair(jsonArray.get(0).getAsString(), jsonArray.get(1).getAsString());
    }
}
