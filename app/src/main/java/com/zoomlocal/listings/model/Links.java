
package com.zoomlocal.listings.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Links implements Serializable {

    private List<Self> self = new ArrayList<Self>();
    private List<Collection> collection = new ArrayList<Collection>();

    public List<Self> getSelf() {
        return self;
    }

    public List<Collection> getCollection() {
        return collection;
    }
}
