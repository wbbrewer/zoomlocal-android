package com.zoomlocal.listings.model.deserializers;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.zoomlocal.listings.model.SizedImage;

import java.lang.reflect.Type;

/**
 */
public class SizedImageDeserializer implements JsonDeserializer<SizedImage> {
    @Override
    public SizedImage deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonArray jsonArray = json.getAsJsonArray();

        return new SizedImage(jsonArray.get(0).getAsString(),
            jsonArray.get(1).getAsInt(),
            jsonArray.get(2).getAsInt(),
            jsonArray.get(3).getAsBoolean());
    }
}
