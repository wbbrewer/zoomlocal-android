
package com.zoomlocal.listings.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Term implements Serializable {

    private long id;
    private int count;
    private String description;
    private String link;
    private String name;
    private String slug;
    private String taxonomy;
    private long parent;
    private Termmeta termmeta = new Termmeta();
    @SerializedName("_links")
    private Links Links = new Links();

    public long getId() {
        return id;
    }

    public int getCount() {
        return count;
    }

    public String getDescription() {
        return description;
    }

    public String getLink() {
        return link;
    }

    public String getName() {
        return name;
    }

    public String getSlug() {
        return slug;
    }

    public String getTaxonomy() {
        return taxonomy;
    }

    public long getParent() {
        return parent;
    }

    public Termmeta getTermmeta() {
        return termmeta;
    }

    public com.zoomlocal.listings.model.Links getLinks() {
        return Links;
    }
}
