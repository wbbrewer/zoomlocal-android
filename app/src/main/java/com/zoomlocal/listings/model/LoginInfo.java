package com.zoomlocal.listings.model;

/**
 */
public class LoginInfo {

    private String accessToken;
    private String refreshToken;

    public LoginInfo() {
    }

    public LoginInfo(String accessToken, String refreshToken) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }
}
