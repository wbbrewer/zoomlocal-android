package com.zoomlocal.listings.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Terms implements Serializable {

    private List<Category> category = new ArrayList<Category>();
    private List<Object> tag = new ArrayList<Object>();
    private Vendor vendor = new Vendor();

    public List<Category> getCategory() {
        return category;
    }

    public List<Object> getTag() {
        return tag;
    }

    public Vendor getVendor() {
        return vendor;
    }
}
