package com.zoomlocal.listings.model;

import java.io.Serializable;

public class Hours implements Serializable {

    private OpenClosePair sun = new OpenClosePair();
    private OpenClosePair mon = new OpenClosePair();
    private OpenClosePair tue = new OpenClosePair();
    private OpenClosePair wed = new OpenClosePair();
    private OpenClosePair thu = new OpenClosePair();
    private OpenClosePair fri = new OpenClosePair();
    private OpenClosePair sat = new OpenClosePair();

    public OpenClosePair getSun() {
        return sun;
    }

    public OpenClosePair getMon() {
        return mon;
    }

    public OpenClosePair getTue() {
        return tue;
    }

    public OpenClosePair getWed() {
        return wed;
    }

    public OpenClosePair getThu() {
        return thu;
    }

    public OpenClosePair getFri() {
        return fri;
    }

    public OpenClosePair getSat() {
        return sat;
    }
}
