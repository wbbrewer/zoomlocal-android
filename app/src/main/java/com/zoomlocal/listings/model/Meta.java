package com.zoomlocal.listings.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Meta implements Serializable {

    @SerializedName("_featured")
    private String featured;
    @SerializedName("_price")
    private String price;
    @SerializedName("_regular_price")
    private String regularPrice;
    @SerializedName("_sale_price")
    private String salePrice;
    @SerializedName("_visibility")
    private String visibility;
    @SerializedName("_rq_event_start_date")
    private String rqEventStartDate;
    @SerializedName("_rq_event_end_date")
    private String rqEventEndDate;
    @SerializedName("_rq_event_country_name")
    private String rqEventCountryName;
    @SerializedName("_rq_event_region_name")
    private String rqEventRegionName;
    @SerializedName("_rq_event_address_name")
    private String rqEventAddressName;
    @SerializedName("_rq_event_zip_code")
    private String rqEventZipCode;
    @SerializedName("_rq_event_lat_name")
    private String rqEventLatName;
    @SerializedName("_rq_event_lon_name")
    private String rqEventLonName;

    public String getFeatured() {
        return featured;
    }

    public String getPrice() {
        return price;
    }

    public String getRegularPrice() {
        return regularPrice;
    }

    public String getSalePrice() {
        return salePrice;
    }

    public String getVisibility() {
        return visibility;
    }

    public String getRqEventStartDate() {
        return rqEventStartDate;
    }

    public String getRqEventEndDate() {
        return rqEventEndDate;
    }

    public String getRqEventCountryName() {
        return rqEventCountryName;
    }

    public String getRqEventRegionName() {
        return rqEventRegionName;
    }

    public String getRqEventAddressName() {
        return rqEventAddressName;
    }

    public String getRqEventZipCode() {
        return rqEventZipCode;
    }

    public String getRqEventLatName() {
        return rqEventLatName;
    }

    public String getRqEventLonName() {
        return rqEventLonName;
    }
}
