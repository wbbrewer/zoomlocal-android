package com.zoomlocal.listings.model;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

public class VendorMeta implements Serializable {

    private Images images = new Images();
    private Images bannerImages = new Images();
    private String name;
    private String description;
    private String owner;
    private String location;
    private String telephone;
    private String storeEmail;
    private String enableSelling;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zip;
    private String website;
    private Hours hours = new Hours();
    private String headerImage;
    private Socials socials = new Socials();
    private LocationHolder geoloc = new LocationHolder();

    public LatLng getLatLon() {
        return geoloc.getLatLon();
    }

    public Images getImages() {
        return images;
    }

    public Images getBannerImages() {
        return bannerImages;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getOwner() {
        return owner;
    }

    public String getLocation() {
        return location;
    }

    public String getTelephone() {
        return telephone;
    }

    public String getStoreEmail() {
        return storeEmail;
    }

    public String getEnableSelling() {
        return enableSelling;
    }

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getZip() {
        return zip;
    }

    public String getWebsite() {
        return website;
    }

    public Hours getHours() {
        return hours;
    }

    public String getHeaderImage() {
        return headerImage;
    }

    public Socials getSocials() {
        return socials;
    }

    public LocationHolder getGeoloc() {
        return geoloc;
    }
}
