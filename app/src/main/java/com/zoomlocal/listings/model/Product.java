package com.zoomlocal.listings.model;

import java.io.Serializable;

public class Product implements Serializable {

    private long id;
    private String date;
    private String link;
    private String modified;
    private String title;
    private String shortDescription;
    private Images images = new Images();
    private Meta meta = new Meta();
    private Terms terms = new Terms();
    private int likes;
    private String isLiked;

    public long getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getLink() {
        return link;
    }

    public String getModified() {
        return modified;
    }

    public String getTitle() {
        return title;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public Images getImages() {
        return images;
    }

    public Meta getMeta() {
        return meta;
    }

    public Terms getTerms() {
        return terms;
    }

    public int getLikes() {
        return likes;
    }

    public String getIsLiked() {
        return isLiked;
    }
}
