package com.zoomlocal.listings.model;

import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.io.Serializable;

/**
 *
 */

public class LocationHolder implements Serializable {
    private double latitude;
    private double longitude;

    public LocationHolder() {
    }

    public LocationHolder(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public LatLng getLatLon() {
        return new LatLng(latitude, longitude);
    }

    public Location getLocation() {
        Location location = new Location("");
        location.setLongitude(longitude);
        location.setLatitude(latitude);
        return location;
    }
}
