package com.zoomlocal.listings.model;

import java.io.Serializable;

/**
 */
public class SizedImage implements Serializable {
    private String url;
    private int width;
    private int height;
    private boolean flag;

    public SizedImage() {
    }

    public SizedImage(String url, int width, int height, boolean flag) {
        this.url = url;
        this.width = width;
        this.height = height;
        this.flag = flag;
    }

    public String getUrl() {
        return url;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isFlag() {
        return flag;
    }
}
