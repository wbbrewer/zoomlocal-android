package com.zoomlocal.listings.model;

import java.io.Serializable;

public class Vendor implements Serializable {

    private long id;
    private int count;
    private String description;
    private String link;
    private String name;
    private String slug;
    private String taxonomy;
    private VendorMeta vendorMeta = new VendorMeta();

    public String getBannerImageUrl() {
        return vendorMeta.getBannerImages().getMedium().getUrl();
    }

    public String getCityState() {
        return String.format("%s, %s", vendorMeta.getCity(), vendorMeta.getState());
    }

    public long getId() {
        return id;
    }

    public int getCount() {
        return count;
    }

    public String getDescription() {
        return description;
    }

    public String getLink() {
        return link;
    }

    public String getName() {
        return name;
    }

    public String getSlug() {
        return slug;
    }

    public String getTaxonomy() {
        return taxonomy;
    }

    public VendorMeta getVendorMeta() {
        return vendorMeta;
    }
}
