package com.zoomlocal.listings.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Date;

/**
 *
 */
public class Coupon implements Serializable {
    private String date;
    private long id;
    private Images images = new Images();
    private String link;
    private CouponMeta meta = new CouponMeta();

    @SerializedName("modified")
    private String modifiedDate;

    private Date redeemingDate;
    private String shortDescription;
    private String title;
    private Terms terms = new Terms();

    public String getDate() {
        return date;
    }

    public long getId() {
        return id;
    }

    public Images getImages() {
        return images;
    }

    public String getLink() {
        return link;
    }

    public CouponMeta getMeta() {
        return meta;
    }

    public String getModifiedDate() {
        return modifiedDate;
    }

    public Date getRedeemingDate() {
        return redeemingDate;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getTitle() {
        return title;
    }

    public Terms getTerms() {
        return terms;
    }
}
