package com.zoomlocal.listings.model;

import java.io.Serializable;

public class Images implements Serializable {

    private SizedImage thumbnail = new SizedImage();
    private SizedImage medium = new SizedImage();
    private SizedImage large = new SizedImage();
    private SizedImage shopThumbnail = new SizedImage();
    private SizedImage shopCatalog = new SizedImage();
    private SizedImage shopSingle = new SizedImage();
    private SizedImage postThumbnail = new SizedImage();

    public SizedImage getThumbnail() {
        return thumbnail;
    }

    public SizedImage getMedium() {
        return medium;
    }

    public SizedImage getLarge() {
        return large;
    }

    public SizedImage getShopThumbnail() {
        return shopThumbnail;
    }

    public SizedImage getShopCatalog() {
        return shopCatalog;
    }

    public SizedImage getShopSingle() {
        return shopSingle;
    }

    public SizedImage getPostThumbnail() {
        return postThumbnail;
    }
}
