package com.zoomlocal.listings.model;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 */

public class CouponMeta implements Serializable {
    private static final String TAG = "CouponMeta";

    @SerializedName("_featured")
    private String featured;

    @SerializedName("_price")
    private String price;

    @SerializedName("_regular_price")
    private String regularPrice;

    @SerializedName("_sale_rice")
    private String salePrice;

    @SerializedName("_visibility")
    private String visibility;

    private String couponEndDate;

    public String getFeatured() {
        return featured;
    }

    public String getPrice() {
        return price;
    }

    public String getRegularPrice() {
        return regularPrice;
    }

    public String getCouponEndDate() {
        return couponEndDate;
    }

    public String getCouponEndDateFormatted() {
        try {
            SimpleDateFormat inputDateFormat = new SimpleDateFormat("yyyy-dd-MM");
            return TextUtils.isEmpty(couponEndDate) ? "" : new SimpleDateFormat("M/d/yy").format(inputDateFormat.parse(couponEndDate));
        } catch (ParseException e) {
            Log.e(TAG, "Cannot reformat date: " + couponEndDate, e);
            return "";
        }
    }

    public String getSalePrice() {
        return salePrice;
    }

}
