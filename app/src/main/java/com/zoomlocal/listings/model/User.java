package com.zoomlocal.listings.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aricbrown on 11/10/15.
 */
public class User {
    private String description;
    private String email;
    private String firstName;
    private long id;
    private String lastName;
    private String link;
    private String name;
    private String nickname;
    private String registeredDate;
    private List<String> roles = new ArrayList<String>();
    private String slug;
    private String url;

    public String getDescription() {
        return description;
    }

    public String getEmail() {
        return email;
    }

    public String getFirstName() {
        return firstName;
    }

    public long getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public String getLink() {
        return link;
    }

    public String getName() {
        return name;
    }

    public String getNickname() {
        return nickname;
    }

    public String getRegisteredDate() {
        return registeredDate;
    }

    public List<String> getRoles() {
        return roles;
    }

    public String getSlug() {
        return slug;
    }

    public String getUrl() {
        return url;
    }
}
