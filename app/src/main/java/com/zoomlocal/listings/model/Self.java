
package com.zoomlocal.listings.model;

import java.io.Serializable;

public class Self implements Serializable {

    private String href;

    public String getHref() {
        return href;
    }
}

