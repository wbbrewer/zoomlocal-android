package com.zoomlocal.listings.model;

import java.io.Serializable;

public class Socials implements Serializable {

    private String facebook;
    private String twitter;
    private String google;
    private String linkedin;
    private String youtube;

    public String getFacebook() {
        return facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public String getGoogle() {
        return google;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public String getYoutube() {
        return youtube;
    }
}
