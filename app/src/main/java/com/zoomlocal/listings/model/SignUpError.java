package com.zoomlocal.listings.model;

import com.google.gson.annotations.SerializedName;

/**
 */
public class SignUpError {
    @SerializedName("code")
    private String errorType;

    @SerializedName("message")
    private String errorDescription;

    public String getErrorType() {
        return errorType;
    }

    public String getErrorDescription() {
        return errorDescription;
    }
}
