package com.zoomlocal.listings;

import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;

import com.zoomlocal.listings.model.LoginInfo;

import java.util.ArrayList;
import java.util.List;

/**
 */
public class UserData {
    private static final String ACCESS_TOKEN_KEY = "ACCESS_TOKEN_KEY";
    private static final String REFRESH_TOKEN_KEY = "REFRESH_TOKEN_KEY";

    private final SharedPreferences preferences;

    public interface OnLoginListener {
        void onLoginStateChanged(boolean isLoggedIn);
    }

    private LoginInfo loginInfo;

    private List<OnLoginListener> listeners;

    public UserData(SharedPreferences preferences) {
        this.preferences = preferences;
        this.listeners = new ArrayList<>();

        if (preferences.contains(ACCESS_TOKEN_KEY)) {
            loginInfo = new LoginInfo(preferences.getString(ACCESS_TOKEN_KEY, ""), preferences.getString(REFRESH_TOKEN_KEY, ""));
        }
    }

    public void login(LoginInfo loginInfo) {
        this.loginInfo = loginInfo;

        preferences.edit()
            .putString(ACCESS_TOKEN_KEY, loginInfo.getAccessToken())
            .putString(REFRESH_TOKEN_KEY, loginInfo.getRefreshToken())
            .apply();

        notifyLoginStateChanged();
    }

    public void logout() {
        this.loginInfo = null;

        preferences.edit()
            .remove(ACCESS_TOKEN_KEY)
            .remove(REFRESH_TOKEN_KEY)
            .apply();

        notifyLoginStateChanged();
    }

    public void addListener(OnLoginListener listener) {
        listeners.add(listener);
    }

    public void removeListener(OnLoginListener listener) {
        listeners.remove(listener);
    }

    public boolean isLoggedIn() {
        return loginInfo != null;
    }

    public String getAccessToken() {
        return isLoggedIn() ? loginInfo.getAccessToken() : "";
    }

    public String getRefreshToken() {
        return isLoggedIn() ? loginInfo.getRefreshToken() : "";
    }

    public String getAccessTokenOrNull() {
        return isLoggedIn() ? loginInfo.getAccessToken() : null;
    }

    private void notifyLoginStateChanged() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                for (OnLoginListener listener : listeners) {
                    listener.onLoginStateChanged(isLoggedIn());
                }
            }
        });
    }
}
