package com.zoomlocal.listings.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Used to access the device registration token.
 * See https://firebase.google.com/docs/cloud-messaging/android/start/ for more details.
 */
public class AppFirebaseInstanceIdService extends FirebaseInstanceIdService {

    private static final String TAG = "AppFirebaseInstanceIdService";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);

        // TODO: Implement this method to send any registration to your app's servers.
        //sendRegistrationToServer(refreshedToken);
    }

}
