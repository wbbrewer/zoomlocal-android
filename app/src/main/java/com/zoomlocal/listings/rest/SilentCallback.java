package com.zoomlocal.listings.rest;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 *
 */

public class SilentCallback<T> implements Callback<T> {
    private static final String TAG = "SilentCallback";

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        Log.d(TAG, "Request succeeded: " + call.request());
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        Log.e(TAG, "Request failed: " + call.request(), t);
    }
}
