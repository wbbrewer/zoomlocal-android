package com.zoomlocal.listings.rest;

import com.zoomlocal.listings.model.LoginInfo;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 */
public interface UserService {
    String CLIENT_ID = "UGJEdVFb0PEJzyt0LEkprzFQM5GFI3";
    String CLIENT_SECRET = "OCKyhXtW9uLNdllGl7kGwMgbC3hjI0";

    @FormUrlEncoded
    @POST("wp-json/rest/v2/users")
    Call<ResponseBody> signUp(
        @Field("first_name") String firstName,
        @Field("last_name") String lastName,
        @Field("user_email") String email,
        @Field("user_zip") String zip,
        @Field("user_pass") String password
    );

    @FormUrlEncoded
    @POST("oauth/token")
    Call<LoginInfo> login(
        @Field("grant_type") String grantType,
        @Field("client_id") String clientID,
        @Field("client_secret") String clientSecret,
        @Field("username") String email,
        @Field("password") String password
    );

    @FormUrlEncoded
    @POST("oauth/token")
    Call<LoginInfo> refreshToken(
        @Field("grant_type") String grantType,
        @Field("client_id") String clientID,
        @Field("client_secret") String clientSecret,
        @Field("refresh_token") String refreshToken
    );

    @FormUrlEncoded
    @POST("wp-json/rest/v2/password_reset")
    Call<ResponseBody> resetPassword(
        @Field("email") String email
    );

    @GET("oauth/destroy")
    Call<ResponseBody> logout(
        @Query("access_token") String accessToken,
        @Query("refresh_token") String refreshToken
    );
}
