package com.zoomlocal.listings.rest;


import android.text.TextUtils;

import com.google.gson.Gson;
import com.zoomlocal.listings.BuildConfig;
import com.zoomlocal.listings.UserData;
import com.zoomlocal.listings.model.LoginInfo;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.zoomlocal.listings.rest.APIService.ACCESS_TOKEN;

/**
 * Created by aricbrown on 11/6/15.
 */
public class ServiceGenerator {
    private static final String TAG = "ServiceGenerator";

    private static final String API_BASE_URL = "https://listings.zoomlocal.com";
//    private static final String API_BASE_URL = "https://listings-dev.zoomlocal.com";

    private APIService apiService;
    private UserService userService;

    public ServiceGenerator(UserData userData, Gson gson) {
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor()
            .setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);

        OkHttpClient userClient = new OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build();
        userService = buildRetrofitService(userClient, gson, UserService.class);

        OkHttpClient apiClient = new OkHttpClient.Builder()
            .addInterceptor(new AuthInterceptor(userData, userService))
            .addInterceptor(loggingInterceptor)
            .build();
        apiService = buildRetrofitService(apiClient, gson, APIService.class);
    }

    private <T> T buildRetrofitService(OkHttpClient client, Gson gson, Class<T> clazz) {
        return new Retrofit.Builder()
            .baseUrl(API_BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
            .create(clazz);
    }

    public APIService getService() {
        return apiService;
    }

    public UserService getUserService() {
        return userService;
    }

    // Authentificator works for 401 only. But our server uses 403
    // http://stackoverflow.com/questions/22450036/refreshing-oauth-token-using-retrofit-without-modifying-all-calls
    // https://futurestud.io/blog/retrofit-2-manage-request-headers-in-okhttp-interceptor
    // http://lgvalle.xyz/2015/07/27/okhttp-authentication/
    private static class AuthInterceptor implements Interceptor {
        private UserData userData;
        private final UserService userService;

        private AuthInterceptor(UserData userData, UserService userService) {
            this.userData = userData;
            this.userService = userService;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            Request originalRequest = chain.request();
            Response origResponse = chain.proceed(originalRequest);

            if (origResponse.code() == 403 && userData.isLoggedIn()) {
                Call<LoginInfo> loginInfoCall = userService.refreshToken("refresh_token", UserService.CLIENT_ID, UserService.CLIENT_SECRET, userData.getRefreshToken());
                retrofit2.Response<LoginInfo> infoResponse = loginInfoCall.execute();

                String expiredAccessToken = userData.getAccessToken();

                if (infoResponse.isSuccessful()) {
                    userData.login(infoResponse.body());
                } else if (infoResponse.code() == 403 || infoResponse.code() == 401) {
                    userData.logout();
                }

                Request.Builder requestWithTokenBuilder = originalRequest.newBuilder();
                if (originalRequest.url().queryParameterNames().contains("access_token")) {
                    // override url parameter with refreshed token
                    HttpUrl url = originalRequest.url().newBuilder()
                        .setQueryParameter(ACCESS_TOKEN, userData.getAccessToken())
                        .build();

                    requestWithTokenBuilder.url(url);
                } else {
                    // look in body
                    String originalBody = bodyToString(originalRequest.body());
                    if (!TextUtils.isEmpty(originalBody) && originalBody.contains(ACCESS_TOKEN)) {
                        // access_token=sqgjy0lkt65ck0jcezuyuyzbis33mdemzbwarwca&redeemed=1
                        String modifiedBody = originalBody.replaceAll(expiredAccessToken, userData.getAccessToken());
                        requestWithTokenBuilder.post(RequestBody.create(MediaType.parse("application/x-www-form-urlencoded;charset=UTF-8"), modifiedBody));
                    }
                }

                return chain.proceed(requestWithTokenBuilder.build());
            } else {
                return origResponse;
            }
        }
    }

    private static String bodyToString(final RequestBody request) {
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            if (copy != null)
                copy.writeTo(buffer);
            else
                return "";
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "";
        }
    }


}
