package com.zoomlocal.listings.rest;

import android.content.Context;

import com.zoomlocal.listings.util.Progressable;

import retrofit2.Call;

/**
 */
public class ProgressedCallback<T> extends DefaultCallback<T>  {
    private final Progressable progressable;

    public ProgressedCallback(Context context, Progressable progressable) {
        super(context);
        this.progressable = progressable;

        progressable.updateProgress(true);
    }

    @Override
    protected void onFinish(Call<T> call, boolean success) {
        progressable.updateProgress(false);
    }
}
