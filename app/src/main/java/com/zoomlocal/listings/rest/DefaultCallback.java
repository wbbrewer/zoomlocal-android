package com.zoomlocal.listings.rest;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.net.UnknownHostException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 */
public class DefaultCallback<T> implements Callback<T> {
    private static final String TAG = "DefaultCallback";

    private Context context;

    public DefaultCallback(Context context) {
        this.context = context;
    }

    @Override
    public final void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            onSuccess(call, response);
        } else {
            onServerError(call, response);
        }
        onFinish(call, response.isSuccessful());
    }

    @Override
    public final void onFailure(Call<T> call, Throwable t) {
        Log.e(TAG, "Failure on request: " + call.request(), t);
        onNetworkError(call, t);
        onFinish(call, false);
    }

    protected void onSuccess(Call<T> call, Response<T> response) {

    }

    protected void onServerError(Call<T> call, Response<T> response) {
        Toast.makeText(context, "Server error occurred", Toast.LENGTH_SHORT).show();
    }

    protected void onNetworkError(Call<T> call, Throwable t) {
        String text = t.getMessage();
        if (t instanceof UnknownHostException) {
            text = "No Internet connection is available.";
        }
        showToast(text);
    }

    protected void onFinish(Call<T> call, boolean success) {

    }

    protected final void showToast(String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }
}
