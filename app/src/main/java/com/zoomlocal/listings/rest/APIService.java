package com.zoomlocal.listings.rest;

import com.zoomlocal.listings.LocalDataManager;
import com.zoomlocal.listings.model.Coupon;
import com.zoomlocal.listings.model.Images;
import com.zoomlocal.listings.model.Product;
import com.zoomlocal.listings.model.Term;
import com.zoomlocal.listings.model.Vendor;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;


/**
 * Created by aricbrown on 11/6/15.
 */
public interface APIService {
    String ACCESS_TOKEN = "access_token";

    /**
     * getListings
     *
     * REQUIRED
     * featured=yes
     * page=x
     * latitude=0.0
     * longitude=0.0
     * radius=100
     *
     * OPTIONAL
     * filter[product_cat]
     */

    @GET("/wp-json/rest/v2/listing?featured=yes&per_page=" + LocalDataManager.PER_PAGE_AMOUNT)
    Call<List<Product>> getListings(
        @Query(ACCESS_TOKEN) String accessToken,
        @Query("page") int page,
        @Query("latitude") double latitude,
        @Query("longitude") double longitude,
        @Query("radius") int radius,
        @Query("filter[product_cat]") String slug
    );

    // Get Parent Categories -> parent=0
    // Get Sub Categories -> parent=term_id

    // hide_empty=true
    // orderby=slug
    // parent=0
    // per_page=100

    @GET("/wp-json/wp/v2/terms/product_cat?hide_empty=true&orderby=slug&per_page=100")
    Call<List<Term>> getCategories(@Query("parent") long parent);

    /**
     * getEvents
     *
     * REQUIRED
     * featured=yes
     * page=x
     * latitude=0.0
     * longitude=0.0
     * radius=100
     *
     * OPTIONAL
     * filter[product_cat]
     */

    @GET("/wp-json/rest/v2/event?featured=yes&per_page=" + LocalDataManager.PER_PAGE_AMOUNT)
    Call<List<Product>> getEvents(
        @Query("page") int page,
        @Query("latitude") double latitude,
        @Query("longitude") double longitude,
        @Query("radius") int radius
    );

    /**
     * wp-json/rest/v2/all
     * featured=yes
     * filter[yith_shop_vendor]=slug
     * page=currentPage
     */

    @GET("/wp-json/rest/v2/all?featured=yes&per_page=" + LocalDataManager.PER_PAGE_AMOUNT)
    Call<List<Product>> getAllProducts(
        @Query("filter[yith_shop_vendor]") String slug,
        @Query("page") int page);

    // Get Vendors for My Area. -> per_page=10
    // Get all Vendors for MapView. -> per_page=0

    // hide_empty=true
    // page=
    // per_page=
    // orderby=slug
    // latitude=
    // longitude=
    // radius=

    // https://listings-dev.zoomlocal.com/wp-json/rest/v2/vendor?hide_empty=true&latitude=30.401667&longitude=-89.076111&page=1&per_page=10&radius=50&products_cat=shopping

    @GET("/wp-json/rest/v2/vendor?hide_empty=true")
    Call<List<Vendor>> getVendors(
        @Query("latitude") double latitude,
        @Query("longitude") double longitude,
        @Query("page") int page,
        @Query("per_page") int perPage,
        @Query("radius") int radius,
        @Query("products_cat") String category);

    @GET("/wp-json/rest/v2/vendor?banner=yes")
    Call<List<Vendor>> getVendorBanners();

    @GET("/wp-json/rest/v2/vendor?hide_empty=true&orderby=slug&per_page=0")
    Call<List<Vendor>> getAllVendors();

    @GET("wp-json/rest/v2/advertisements")
    Call<List<Images>> getAdvertisements();

    @GET("/wp-json/rest/v2/event?per_page=0")
    Call<List<Product>> searchEvents(
        @Query("filter[s]") String filter
    );

    @FormUrlEncoded
    @POST("/wp-json/rest/v2/listing/{id}")
    Call<Product> likeProduct(
        @Path("id") long id,
        @Field(ACCESS_TOKEN) String accessToken,
        @Field("is_liked") int value
    );

    @GET("/wp-json/rest/v2/listing/{id}")
    Call<Product> productById(
        @Path("id") long id,
        @Query(ACCESS_TOKEN) String accessToken
    );

    @GET("/wp-json/rest/v2/listing?per_page=0")
    Call<List<Product>> searchListing(
        @Query("filter[s]") String filter,
        @Query("latitude") double latitude,
        @Query("longitude") double longitude
    );

    @GET("/wp-json/rest/v2/vendor?per_page=0")
    Call<List<Vendor>> searchVendor(
        @Query("search") String filter
    );

    @GET("/wp-json/rest/v2/coupons?featured=yes&per_page=" + LocalDataManager.PER_PAGE_AMOUNT)
    Call<List<Coupon>> getCoupons(
        @Query(ACCESS_TOKEN) String accessToken,
        @Query("page") int page,
        @Query("latitude") double latitude,
        @Query("longitude") double longitude,
        @Query("radius") int radius
    );

    @GET("/wp-json/rest/v2/coupons?featured=yes&per_page=" + LocalDataManager.PER_PAGE_AMOUNT)
    Call<List<Coupon>> getCouponsForVendor(
        @Query(ACCESS_TOKEN) String accessToken,
        @Query("filter[yith_shop_vendor]") String vendorSlug,
        @Query("page") int page
    );

    @GET("/wp-json/rest/v2/coupons/{id}")
    Call<Coupon> getCoupon(
        @Path("id") long id,
        @Query(ACCESS_TOKEN) String accessToken
    );

    @FormUrlEncoded
    @POST("/wp-json/rest/v2/coupons/{id}")
    Call<Coupon> redeemCoupon(
        @Path("id") long id,
        @Field(ACCESS_TOKEN) String accessToken,
        @Field("redeemed") int redeemValue
    );

    @GET
    Call<ResponseBody> downloadFile(@Url String fileUrl);
}
