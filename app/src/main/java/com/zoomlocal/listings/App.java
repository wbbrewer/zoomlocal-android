package com.zoomlocal.listings;

import android.app.Application;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.SaveCallback;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.zoomlocal.listings.model.LocationHolder;
import com.zoomlocal.listings.model.OpenClosePair;
import com.zoomlocal.listings.model.SizedImage;
import com.zoomlocal.listings.model.deserializers.LocationHolderDeserializer;
import com.zoomlocal.listings.model.deserializers.OpenClosePairDeserializer;
import com.zoomlocal.listings.model.deserializers.SizedImageDeserializer;
import com.zoomlocal.listings.rest.APIService;
import com.zoomlocal.listings.rest.ServiceGenerator;
import com.zoomlocal.listings.rest.UserService;
import com.zoomlocal.listings.ui.MainActivity;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;
import io.fabric.sdk.android.Kit;

/**
 * Created by aricbrown on 11/10/15.
 */
public class App extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "IVwNYqXY8NY0VTWHTFj258IZ2";
    private static final String TWITTER_SECRET = "unvv6Vn7noJKca29tZAiG8dHh7yhMuLtmty2EdLLuEk6Gpf3kI";

    private static final String TAG = "App";

    private static App instance;

    public static Resources getResourcesStatic() {
        return instance.getResources();
    }

    private UserData userData;
    private AdAfterSplashDownloader mAdAfterSplashDownloader;
    private ServiceGenerator serviceGenerator;
    private LocalDataManager localDataManager;
    private Gson gson;

    @Override
    public void onCreate() {
        super.onCreate();
        List<Kit> kits = new ArrayList<>();
        if (!BuildConfig.DEBUG) {
            kits.add(new Crashlytics());
        }
        kits.add(new Twitter(new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET)));
        Fabric.with(this, kits.toArray(new Kit[0]));

        instance = this;
        FacebookSdk.sdkInitialize(getApplicationContext());

        setUpParseSDK();

        gson = new GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .setDateFormat("yyyy-MM-dd hh:mm:ss")
            .registerTypeAdapter(SizedImage.class, new SizedImageDeserializer())
            .registerTypeAdapter(OpenClosePair.class, new OpenClosePairDeserializer())
            .registerTypeAdapter(LocationHolder.class, new LocationHolderDeserializer())
            .registerTypeAdapterFactory(new NullSafeTypeAdapterFactory())
            .create();

        userData = new UserData(PreferenceManager.getDefaultSharedPreferences(this));
        serviceGenerator = new ServiceGenerator(userData, gson);
        localDataManager = new LocalDataManager(this);

        mAdAfterSplashDownloader = new AdAfterSplashDownloader();

        MainActivity.setUpMobileDeeplinkingSDK(this);

        // Debug code for deep linking
//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                String deepLink = "zoomlocal://listings";
//                Intent intent = new Intent(Intent.ACTION_VIEW);
//                intent.setData(Uri.parse(deepLink));
//                startActivity(intent);
//            }
//        }, 10000);

    }


    public static App getInstance() {
        return instance;
    }

    public UserData getUserData() {
        return userData;
    }

    public AdAfterSplashDownloader getAdAfterSplashDownloader() {
        return mAdAfterSplashDownloader;
    }

    public APIService getAPIService() {
        return serviceGenerator.getService();
    }

    public UserService getUserService() {
        return serviceGenerator.getUserService();
    }

    public LocalDataManager getLocalDataManager() {
        return localDataManager;
    }

    public Gson getGson() {
        return gson;
    }

    private void setUpParseSDK() {
        Parse.setLogLevel(Log.VERBOSE);

        // Details on link https://github.com/ParsePlatform/parse-server/wiki/Parse-Server-Guide#keys
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId("6b1b372e-2347-4eb8-8608-6e365b8fbd78")
                .clientKey(null)
                .server("https://api.parse.buddy.com/parse/")
                .build()
        );

        ParseInstallation.getCurrentInstallation().saveInBackground(
                new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            String deviceToken = (String) ParseInstallation.getCurrentInstallation().get("deviceToken");
                            Log.d(TAG, "Device has been registered on Parse with device token: " + deviceToken);
                        } else {
                            Log.e(TAG, "Parse installation failed: " + e.getMessage(), e);
                        }
                    }
                }
        );

        ParsePush.subscribeInBackground("global", new SaveCallback()
        {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d(TAG, "Parse: successfully subscribed to the \"global\" channel.");
                } else {
                    Log.e(TAG, "Parse: failed to subscribe for push", e);
                }
            }
        });
    }

    private class NullSafeTypeAdapterFactory implements TypeAdapterFactory {

        @Override
        public <T> TypeAdapter<T> create(Gson gson, final TypeToken<T> type) {
            final TypeAdapter<T> delegate = gson.getDelegateAdapter(this, type);
            return new TypeAdapter<T>() {
                @Override
                public void write(JsonWriter out, T value) throws IOException {
                    delegate.write(out, value);
                }

                @Override
                public T read(JsonReader in) throws IOException {
                    T read = delegate.read(in);

                    if (read == null && type.getRawType().getName().startsWith("com.zoomlocal.listings.model")) {
                        Log.d(TAG, "Null detected! type: " + type);
                        try {
                            read = (T) type.getRawType().newInstance();
                        } catch (Exception e) {
                            Log.e(TAG, "Cannot instantiate raw type: " + type.getRawType(), e);
                        }
                    }

                    return read;
                }
            };
        }
    }
}
