package com.zoomlocal.listings;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;

import com.zoomlocal.listings.util.Util;

import io.nlopez.smartlocation.SmartLocation;

/**
 *
 */

public class LocalDataManager {
    public static final int PER_PAGE_AMOUNT = 10;
    private static final String RADIUS_KEY = "RADIUS_KEY";

    public enum Radius {
        MILES_1(1),
        MILES_5(5),
        MILES_10(10),
        MILES_25(25),
        MILES_50(50),
        MILES_100(100),
        MILES_250(250),
        MILES_500(500),
        MILES_1000(1000);

        int radiusMiles;

        Radius(int radiusMiles) {
            this.radiusMiles = radiusMiles;
        }
    }

    private Context context;
    private SharedPreferences preferences;

    private Location centerLocation;

    public LocalDataManager(Context context) {
        this.context = context;
        preferences = PreferenceManager.getDefaultSharedPreferences(context);

        centerLocation = new Location("");
        centerLocation.setLatitude(30.4016666667);
        centerLocation.setLongitude(-89.07611111111);
    }

    public Location getLocation() {
        if (BuildConfig.DEBUG || !Util.locationPermissionGranted(context)) {
            return centerLocation;
        } else {
            Location lastLocation = SmartLocation.with(context).location().getLastLocation();
            return lastLocation == null ? centerLocation : lastLocation;
        }
    }

    public int getRadiusMiles() {
        return getRadius().radiusMiles;
    }

    public Radius getRadius() {
        return Radius.values()[preferences.getInt(RADIUS_KEY, Radius.MILES_50.ordinal())];
    }

    public void setRadius(Radius radius) {
        preferences.edit().putInt(RADIUS_KEY, radius.ordinal()).apply();
    }
}
