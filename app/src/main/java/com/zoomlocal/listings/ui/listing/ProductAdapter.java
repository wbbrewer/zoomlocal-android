
package com.zoomlocal.listings.ui.listing;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoomlocal.listings.R;
import com.zoomlocal.listings.databinding.ItemProductBinding;
import com.zoomlocal.listings.model.Product;
import com.zoomlocal.listings.ui.PaginationAdapter;
import com.zoomlocal.listings.ui.listing.details.ProductDetailsActivity;

/**
 * Created by aricbrown on 11/9/15.
 */

public class ProductAdapter extends PaginationAdapter<Product, ProductAdapter.BindingHolder> {

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemProductBinding itemProductBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.getContext()),
            R.layout.item_product,
            parent,
            false);
        return new BindingHolder(itemProductBinding);
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        ItemProductBinding binding = holder.binding;
        ItemProductViewModel viewModel = new ItemProductViewModel(getItem(position));
        binding.setViewModel(viewModel);
    }

    public static class BindingHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ItemProductBinding binding;

        public BindingHolder(ItemProductBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            binding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Intent detailIntent = new Intent(view.getContext(), ProductDetailsActivity.class);
            detailIntent.putExtra(ProductDetailsActivity.EXTRA_MODEL, binding.getViewModel().getProduct());
            view.getContext().startActivity(detailIntent);
        }
    }
}
