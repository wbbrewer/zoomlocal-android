package com.zoomlocal.listings.ui.home;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.zoomlocal.listings.App;
import com.zoomlocal.listings.R;
import com.zoomlocal.listings.model.Vendor;
import com.zoomlocal.listings.rest.ProgressedCallback;
import com.zoomlocal.listings.util.PageIndicator;
import com.zoomlocal.listings.util.Progressable;
import com.zoomlocal.listings.util.ViewPagerAdapter;
import com.zoomlocal.listings.util.ViewProgressable;
import com.zoomlocal.listings.ui.vendor.details.VendorActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Response;

/**
 *
 */
public class HomeFragment extends Fragment {

    private static final Long BANNER_CHANGE_DELAY = TimeUnit.SECONDS.toMillis(5);

    private static final List<HomeItem> DATA = new ArrayList<HomeItem>() {{
        add(new HomeItem("COUPONS", R.drawable.ic_coupons_category));
        add(new HomeItem("SHOPPING", R.drawable.ic_shopping_category));
        add(new HomeItem("DINING", R.drawable.ic_dining_category));
        add(new HomeItem("SERVICES", R.drawable.ic_services_category));
        add(new HomeItem("RECREATION", R.drawable.ic_recreation_category));
        add(new HomeItem("NIGHTLIFE", R.drawable.ic_nightlife_category));
        add(new HomeItem("ENTERTAINMENT", R.drawable.ic_entertainment_category));
        add(new HomeItem("EVENTS", R.drawable.ic_events_category));
        add(new HomeItem("SEARCH", R.drawable.ic_search));
    }};

    private ViewPager viewPager;
    private PagerAdapter pagerAdapter;
    private Timer timer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        viewPager = (ViewPager) rootView.findViewById(R.id.bannerViewPager);
        pagerAdapter = new PagerAdapter();
        viewPager.setAdapter(pagerAdapter);
        ((PageIndicator) rootView.findViewById(R.id.circlePageIndicator)).setViewPager(viewPager);

        viewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    scheduleBannerTimer();
                } else {
                    stopBannerTimer();
                }
                return false;
            }
        });

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerView.setLayoutManager(gridLayoutManager);

        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                // element on the top has span 2
                return position == 0 ? 2 : 1;
            }
        });

        recyclerView.setAdapter(new HomeAdapter(DATA));

        Call<List<Vendor>> call = App.getInstance().getAPIService().getVendorBanners();
        call.enqueue(new BannersResponseHandler(rootView.getContext(), new ViewProgressable(rootView.findViewById(R.id.progressView))));

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        scheduleBannerTimer();
    }

    @Override
    public void onPause() {
        super.onPause();
        stopBannerTimer();
    }

    private void scheduleBannerTimer() {
        if (timer == null) {
            timer = new Timer();
            timer.schedule(new BannerAutoScrollerTask(), BANNER_CHANGE_DELAY, BANNER_CHANGE_DELAY);
        }
    }

    private void stopBannerTimer() {
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private class BannersResponseHandler extends ProgressedCallback<List<Vendor>> {
        public BannersResponseHandler(Context context, Progressable progressable) {
            super(context, progressable);
        }

        @Override
        protected void onSuccess(Call<List<Vendor>> call, Response<List<Vendor>> response) {
            pagerAdapter.setData(response.body());
        }
    }

    private class BannerAutoScrollerTask extends TimerTask {

        @Override
        public void run() {
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    if (pagerAdapter.getCount() > 0) {
                        viewPager.setCurrentItem((viewPager.getCurrentItem() + 1) % pagerAdapter.getCount());
                    }
                }
            });
        }
    }

    private class PagerAdapter extends ViewPagerAdapter<Vendor> {

        @Override
        public View createView(Context context, Vendor vendor) {
            ImageView imageView = new ImageView(context);
            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);

            String imageUrl = vendor.getBannerImageUrl();
            if (TextUtils.isEmpty(imageUrl)) {
                Picasso.with(context)
                    .load(R.drawable.ic_zl_logo)
                    .into(imageView);
            } else {
                Picasso.with(context)
                    .load(imageUrl)
                    .into(imageView);
            }

            imageView.setOnClickListener(new BannerClickHandler(vendor));

            return imageView;
        }
    }

    private class BannerClickHandler implements View.OnClickListener {
        private Vendor vendor;

        public BannerClickHandler(Vendor vendor) {
            this.vendor = vendor;
        }

        @Override
        public void onClick(View view) {
            Intent detailIntent = new Intent(view.getContext(), VendorActivity.class);
            detailIntent.putExtra(VendorActivity.EXTRA_MODEL, (Vendor) vendor);
            startActivity(detailIntent);
        }
    }
}
