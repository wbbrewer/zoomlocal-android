package com.zoomlocal.listings.ui.home;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoomlocal.listings.ui.MainActivity;
import com.zoomlocal.listings.R;
import com.zoomlocal.listings.databinding.ItemHomeBinding;
import com.zoomlocal.listings.ui.FragmentHolderActivity;
import com.zoomlocal.listings.ui.search.SearchActivity;
import com.zoomlocal.listings.ui.vendor.list.VendorsFragment;

import java.util.List;

/**
 *
 */

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.RecyclerViewHolders> {

    private List<HomeItem> itemList;

    public HomeAdapter(List<HomeItem> itemList) {
        this.itemList = itemList;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemHomeBinding itemProductBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.getContext()),
            R.layout.item_home,
            parent,
            false);
        return new RecyclerViewHolders(itemProductBinding);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        ItemHomeViewModel viewModel = new ItemHomeViewModel(itemList.get(position), position % 2 != 0, itemList.size() - position > 2);
        holder.itemHomeBinding.setViewModel(viewModel);
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ItemHomeBinding itemHomeBinding;

        public RecyclerViewHolders(ItemHomeBinding itemHomeBinding) {
            super(itemHomeBinding.cardView);
            this.itemHomeBinding = itemHomeBinding;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            HomeItem homeItem = itemList.get(getAdapterPosition());

            switch (homeItem.getName()) {
                case "COUPONS":
                    ((MainActivity) view.getContext()).selectNavigationItem(R.id.nav_deals);
                    break;
                case "EVENTS":
                    ((MainActivity) view.getContext()).selectNavigationItem(R.id.nav_events);
                    break;
                case "SEARCH":
                    view.getContext().startActivity(new Intent(view.getContext(), SearchActivity.class));
                    break;
                default:
                    new FragmentHolderActivity.Builder(view.getContext(), VendorsFragment.class)
                        .title(homeItem.getName())
                        .bundleString(VendorsFragment.CATEGORY, homeItem.getName())
                        .buildAndStart();
            }
        }
    }
}
