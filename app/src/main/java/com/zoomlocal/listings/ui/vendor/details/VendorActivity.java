package com.zoomlocal.listings.ui.vendor.details;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.zoomlocal.listings.R;
import com.zoomlocal.listings.databinding.ActivityVendorDetailBinding;
import com.zoomlocal.listings.model.Vendor;
import com.zoomlocal.listings.ui.BaseActivity;

public class VendorActivity extends BaseActivity {

    public static final String EXTRA_MODEL = "extra.model";
    private Vendor mModel;

    private Toolbar mToolbar;

    private ViewPager mPager;
    private VendorPagerAdapter mAdapter;
    private TabLayout mTabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mModel = (Vendor) getIntent().getSerializableExtra(EXTRA_MODEL);

        ActivityVendorDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_vendor_detail);
        binding.setViewModel(new VendorDetailViewModel(mModel));

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mAdapter = new VendorPagerAdapter(this, getSupportFragmentManager(), mModel);
        mPager = (ViewPager) findViewById(R.id.view_pager);
        mPager.setAdapter(mAdapter);
        mPager.setOffscreenPageLimit(2);
        mTabLayout.setTabsFromPagerAdapter(mAdapter);

        //Notice how The Tab Layout adn View Pager object are linked
        mTabLayout.setupWithViewPager(mPager);
        mPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.share, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
              finish();
            return true;
        } else if (id == R.id.share) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = getString(R.string.share_body) + "\n\n" + mModel.getVendorMeta().getImages().getMedium().getUrl();
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.app_name));
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
