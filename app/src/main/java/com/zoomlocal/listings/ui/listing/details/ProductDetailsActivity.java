package com.zoomlocal.listings.ui.listing.details;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.zoomlocal.listings.App;
import com.zoomlocal.listings.R;
import com.zoomlocal.listings.UserData;
import com.zoomlocal.listings.databinding.ActivityItemDetailBinding;
import com.zoomlocal.listings.model.Product;
import com.zoomlocal.listings.rest.APIService;
import com.zoomlocal.listings.rest.DefaultCallback;
import com.zoomlocal.listings.ui.BaseActivity;

import retrofit2.Call;
import retrofit2.Response;

public class ProductDetailsActivity extends BaseActivity {

    public static final String EXTRA_MODEL = "extra.model";
    private Product product;

    private APIService apiService = App.getInstance().getAPIService();
    private UserData userData = App.getInstance().getUserData();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        product = (Product) getIntent().getSerializableExtra(EXTRA_MODEL);

        final ActivityItemDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_item_detail);
        binding.setViewModel(new ProductDetailViewModel(this, product));
        binding.socialShareView.setProduct(product);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Call<Product> productCall = apiService.productById(product.getId(), userData.getAccessToken());
        productCall.enqueue(new DefaultCallback<Product>(this) {
            @Override
            protected void onSuccess(Call<Product> call, Response<Product> response) {
                binding.setViewModel(new ProductDetailViewModel(ProductDetailsActivity.this, response.body()));
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.share, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        } else if (id == R.id.share) {
            Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            String shareBody = getString(R.string.share_body) + "\n\n" + product.getImages().getMedium().getUrl();
            sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.app_name));
            sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
            startActivity(Intent.createChooser(sharingIntent, "Share via"));
        }
        return super.onOptionsItemSelected(item);
    }
}
