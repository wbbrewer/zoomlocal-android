package com.zoomlocal.listings.ui.vendor;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.zoomlocal.listings.App;
import com.zoomlocal.listings.LocalDataManager;
import com.zoomlocal.listings.R;
import com.zoomlocal.listings.model.Vendor;
import com.zoomlocal.listings.rest.APIService;
import com.zoomlocal.listings.rest.DefaultCallback;
import com.zoomlocal.listings.ui.FragmentHolderActivity;
import com.zoomlocal.listings.ui.vendor.details.VendorActivity;
import com.zoomlocal.listings.ui.vendor.list.VendorsFragment;
import com.zoomlocal.listings.util.Util;

import java.util.List;

import retrofit2.Call;
import retrofit2.Response;

/**
 */
public class VendorsOnMapFragment extends SupportMapFragment implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private GoogleMap googleMap;
    private List<Vendor> vendors;

    private APIService apiService = App.getInstance().getAPIService();
    private LocalDataManager localDataManager = App.getInstance().getLocalDataManager();

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setHasOptionsMenu(true);

        getMapAsync(this);

        Call<List<Vendor>> allVendors = apiService.getAllVendors();
        allVendors.enqueue(new DefaultCallback<List<Vendor>>(getContext()) {
            @Override
            protected void onSuccess(Call<List<Vendor>> call, Response<List<Vendor>> response) {
                vendors = response.body();
                updateVendors();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.vendors, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        new FragmentHolderActivity.Builder(getActivity(), VendorsFragment.class)
            .title("Vendors")
            .buildAndStart();
        return true;
    }

    @Override
    @SuppressWarnings({"ResourceType"})
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;

        if (Util.locationPermissionGranted(getActivity())) {
            googleMap.setMyLocationEnabled(true);
        }

        updateVendors();
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Vendor vendor = (Vendor) marker.getTag();
        startActivity(new Intent(getActivity(), VendorActivity.class).putExtra(VendorActivity.EXTRA_MODEL, vendor));
    }

    private void updateVendors() {
        if (googleMap != null && vendors != null) {

            LatLngBounds.Builder boundsBuilder = new LatLngBounds.Builder();
            for (Vendor vendor : vendors) {
                boundsBuilder.include(vendor.getVendorMeta().getLatLon());

                googleMap.addMarker(new MarkerOptions()
                    .position(vendor.getVendorMeta().getLatLon())
                    .title(vendor.getName())
                    .snippet(vendor.getVendorMeta().getAddress1()))
                    .setTag(vendor);
            }
            boundsBuilder.include(Util.toLatLng(localDataManager.getLocation()));

            googleMap.setOnInfoWindowClickListener(this);

            googleMap.animateCamera(CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), 10), 1000, null);
        }
    }
}
