package com.zoomlocal.listings.ui.listing.details;

import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.zoomlocal.listings.App;
import com.zoomlocal.listings.BR;
import com.zoomlocal.listings.R;
import com.zoomlocal.listings.UserData;
import com.zoomlocal.listings.model.Product;
import com.zoomlocal.listings.rest.APIService;
import com.zoomlocal.listings.rest.ProgressedCallback;
import com.zoomlocal.listings.ui.login.LoginSignUpDialogFragment;
import com.zoomlocal.listings.ui.vendor.details.VendorActivity;
import com.zoomlocal.listings.util.ViewProgressable;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by aricbrown on 11/6/15.
 */
public class ProductDetailViewModel extends BaseObservable  {
    private final ProductDetailsActivity activity;
    public Product product;
    private UserData userData = App.getInstance().getUserData();
    private APIService apiService = App.getInstance().getAPIService();

    public ProductDetailViewModel(ProductDetailsActivity activity, Product product) {
        this.activity = activity;
        this.product = product;
    }

    public Boolean isEvent() {
        if (product != null && product.getMeta() != null && product.getMeta().getRqEventStartDate() != null) {
            if (product.getMeta().getRqEventStartDate().length() > 0) {
                // event
                return true;
            }
        }
        return false;
    }

    public String getTitle() {
        return product.getTitle();
    }

    public String getDescription() {
        return product.getShortDescription();
    }

    public String getImageUrl() {
        return product.getImages().getMedium().getUrl();
    }

    public String getStartDate() {
        if (isEvent()) {
            return formatDateFromString(product.getMeta().getRqEventStartDate());
        }
        return "";
    }

    public String getEndDate() {
        if (isEvent()) {
            return formatDateFromString(product.getMeta().getRqEventStartDate());
        }
        return "";
    }

    public String getVenue() {
        if (isEvent()) {
            return String.format("%s, %s", product.getMeta().getRqEventAddressName(), product.getMeta().getRqEventRegionName());
        }
        return "";
    }

    public String getEventDateRange() {
        if (isEvent()) {
            if (product != null && product.getMeta() != null && product.getMeta().getRqEventEndDate() != null) {
                if (product.getMeta().getRqEventEndDate().length() > 0) {
                    return String.format("%s - %s", formatDateFromString(product.getMeta().getRqEventStartDate()), formatDateFromString(product.getMeta().getRqEventEndDate()));
                }
            }

            return formatDateFromString(product.getMeta().getRqEventStartDate());
        }
        return "";
    }

    public String getPrice() {

        return product.getMeta().getPrice();
    }

    public String getVendorImage() {
        return product.getTerms().getVendor().getVendorMeta().getImages().getMedium().getUrl();
    }


    public String getVendorName() {
        return product.getTerms().getVendor().getVendorMeta().getName();
    }

    public String getVendorAddress() {
        return product.getTerms().getVendor().getVendorMeta().getAddress1();

    }

    public String getVendorCityState() {
        return product.getTerms().getVendor().getVendorMeta().getCity() + "," + product.getTerms().getVendor().getVendorMeta().getState();
    }

    public void onClickRetailerDetail(View view) {
        Intent detailIntent = new Intent(view.getContext(), VendorActivity.class);
        detailIntent.putExtra(VendorActivity.EXTRA_MODEL, product.getTerms().getVendor());
        view.getContext().startActivity(detailIntent);
    }

    public String formatDateFromString(String dtStart) {
        Date date = null;
        //String dtStart = "2010-10-15T09:27:37Z";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        try {
            date = format.parse(dtStart);
        } catch (ParseException e) {
            Log.d("test", "Cannot parse date: " + dtStart, e);
        }
        return DateFormat.getDateInstance(DateFormat.SHORT).format(date);
    }

    @Bindable
    public String getLikeButtonText() {
        return TextUtils.isEmpty(product.getIsLiked()) || "false".equalsIgnoreCase(product.getIsLiked())
            ? "Like"
            : "Unlike";
    }

    @Bindable
    public String getLikeDescription() {
        return product.getLikes() == 0
            ? "Be The First"

            : TextUtils.isEmpty(product.getIsLiked())
            ? String.format("%s people like this", product.getLikes())

            : "true".equalsIgnoreCase(product.getIsLiked())
            ? String.format("You and %s others like this", product.getLikes() - 1)
            : String.format("%s people like this", product.getLikes());
    }

    public void onLikeClicked(View view) {
        if (!userData.isLoggedIn()) {
            new LoginSignUpDialogFragment().show(activity.getSupportFragmentManager(), "dialog");
        } else {
            int doLike = TextUtils.isEmpty(product.getIsLiked()) || "false".equalsIgnoreCase(product.getIsLiked()) ? 1 : 0;

            Call<Product> likeCall = apiService.likeProduct(product.getId(), userData.getAccessToken(), doLike);
            likeCall.enqueue(new ProgressedCallback<Product>(view.getContext(), new ViewProgressable(activity.findViewById(R.id.likeProgress))){
                @Override
                protected void onSuccess(Call<Product> call, Response<Product> response) {
                    product = response.body();

                    notifyPropertyChanged(BR.likeButtonText);
                    notifyPropertyChanged(BR.likeDescription);
                }
            });
        }
    }

    private void updateLikeStatus() {

    }
}
