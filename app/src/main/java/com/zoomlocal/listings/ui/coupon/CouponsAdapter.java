package com.zoomlocal.listings.ui.coupon;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoomlocal.listings.R;
import com.zoomlocal.listings.databinding.ItemCouponBinding;
import com.zoomlocal.listings.model.Coupon;
import com.zoomlocal.listings.ui.PaginationAdapter;
import com.zoomlocal.listings.ui.coupon.details.CouponDetailsActivity;

/**
 *
 */

public class CouponsAdapter extends PaginationAdapter<Coupon, CouponsAdapter.RecyclerViewHolders> {

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemCouponBinding itemCouponBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.getContext()),
            R.layout.item_coupon,
            parent,
            false);
        return new CouponsAdapter.RecyclerViewHolders(itemCouponBinding);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        holder.itemCouponBinding.setViewModel(new ItemCouponViewModel(getItem(position)));
    }

    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ItemCouponBinding itemCouponBinding;

        public RecyclerViewHolders(ItemCouponBinding itemCouponBinding) {
            super(itemCouponBinding.getRoot());
            this.itemCouponBinding = itemCouponBinding;
            itemCouponBinding.getRoot().setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            view.getContext().startActivity(
                new Intent(view.getContext(), CouponDetailsActivity.class)
                    .putExtra(CouponDetailsActivity.COUPON, itemCouponBinding.getViewModel().getCoupon())
            );
        }
    }
}
