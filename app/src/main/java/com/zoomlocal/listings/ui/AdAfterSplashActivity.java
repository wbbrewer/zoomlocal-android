package com.zoomlocal.listings.ui;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.zoomlocal.listings.App;
import com.zoomlocal.listings.AdAfterSplashDownloader;
import com.zoomlocal.listings.R;

/**
 * Created by yklishevich on 31/01/17.
 */

public class AdAfterSplashActivity extends BaseActivity {

    private static final String TAG = "MainActivity";
    private ImageView mAdImageView;
    private Button mCloseButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ad_after_splash);
        mAdImageView = (ImageView) findViewById(R.id.imageView);
        mCloseButton = (Button) findViewById(R.id.close_button);

        mCloseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishActivityAnimatedly();
            }
        });

        AdAfterSplashDownloader adAfterSplashDownloader = App.getInstance().getAdAfterSplashDownloader();
        mAdImageView.setImageBitmap(adAfterSplashDownloader.getAdAfterSplashBitmap());
        adAfterSplashDownloader.setAdAfterSplashBitmap(null);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                finishActivityAnimatedly();
            }
        }, 3500);
    }

    private void finishActivityAnimatedly() {
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
