package com.zoomlocal.listings.ui.login;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.v4.app.DialogFragment;
import android.view.View;

import com.zoomlocal.listings.BR;

import com.zoomlocal.listings.util.Progressable;


/**
 *
 */

public class LoginSignUpViewModel extends BaseObservable implements Progressable {
    enum State {
        LOGIN, SIGN_UP, RESET
    }

    private DialogFragment dialogFragment;
    private State state = State.LOGIN;
    private boolean isProgressShown;

    public LoginSignUpViewModel(DialogFragment dialogFragment) {
        this.dialogFragment = dialogFragment;
    }

    public void onCloseClicked(View view) {
        close();
    }

    public void onBackClicked(View view) {
        updateCurrentScreen(State.LOGIN);
    }

    @Bindable
    public int getLoginVisible() {
        return state == State.LOGIN ? View.VISIBLE : View.GONE;
    }

    @Bindable
    public int getSignUpVisible() {
        return state == State.SIGN_UP ? View.VISIBLE : View.GONE;
    }

    @Bindable
    public int getResetVisible() {
        return state == State.RESET ? View.VISIBLE : View.GONE;
    }

    @Bindable
    public int getBackVisible() {
        return state == State.SIGN_UP || state == State.RESET ? View.VISIBLE : View.GONE;
    }

    @Bindable
    public boolean isProgressShown() {
        return isProgressShown;
    }

    public void updateCurrentScreen(State state) {
        this.state = state;

        notifyPropertyChanged(BR.loginVisible);
        notifyPropertyChanged(BR.signUpVisible);
        notifyPropertyChanged(BR.resetVisible);
        notifyPropertyChanged(BR.backVisible);
    }

    public void updateProgress(boolean isVisible) {
        isProgressShown = isVisible;

        notifyPropertyChanged(BR.progressShown);
    }

    public void close() {
        dialogFragment.dismiss();
    }
}
