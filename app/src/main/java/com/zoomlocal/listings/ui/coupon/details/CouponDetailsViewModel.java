package com.zoomlocal.listings.ui.coupon.details;

import android.content.DialogInterface;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.zoomlocal.listings.BR;
import com.zoomlocal.listings.ui.login.LoginSignUpDialogFragment;
import com.zoomlocal.listings.util.Progressable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 *
 */

public class CouponDetailsViewModel extends BaseObservable implements Progressable {
    private static final DateFormat DATE_FORMAT = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);

    private boolean isLoggedIn;
    private boolean showProgress;
    private boolean showImageProgress = true;
    private Date redeemingDate;
    private CouponDetailsActivity couponDetailsActivity;

    public CouponDetailsViewModel(String imageUrl, Date redeemingDate, CouponDetailsActivity couponDetailsActivity, final ImageView imageView) {
        this.redeemingDate = redeemingDate;
        this.couponDetailsActivity = couponDetailsActivity;

        Picasso.with(imageView.getContext())
            .load(imageUrl)
            .into(imageView, new Callback() {
                @Override
                public void onSuccess() {
                    new PhotoViewAttacher(imageView).update();
                    updateImageProgress(false);
                }

                @Override
                public void onError() {
                    updateImageProgress(false);
                    Toast.makeText(imageView.getContext(), "Cannot load coupon image. Please try again later.", Toast.LENGTH_LONG).show();
                }
            });
    }

    public void onActionClicked(final View view) {
        if (!isLoggedIn) {
            new LoginSignUpDialogFragment().show(couponDetailsActivity.getSupportFragmentManager(), "dialog");
        } else if (redeemingDate == null) {
            new AlertDialog.Builder(view.getContext())
                .setTitle("REDEEM COUPON")
                .setMessage("Present to Vendor. By clicking Accept You are granting this Coupon")
                .setNegativeButton("Cancel", null)
                .setPositiveButton("Accept", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        couponDetailsActivity.sendRedeemRequest();
                    }
                })
                .create()
                .show();
        }
    }

    public void setLoggedIn(boolean loggedIn) {
        isLoggedIn = loggedIn;

        notifyPropertyChanged(BR.redeemInfo);
        notifyPropertyChanged(BR.actionText);
    }

    @Override
    public void updateProgress(boolean isVisible) {
        showProgress = isVisible;

        notifyPropertyChanged(BR.showProgress);
    }

    private void updateImageProgress(boolean isVisible) {
        showImageProgress = isVisible;

        notifyPropertyChanged(BR.showImageProgress);
    }

    public Date getRedeemingDate() {
        return redeemingDate;
    }

    public void setRedeemingDate(Date redeemingDate) {
        this.redeemingDate = redeemingDate;

        notifyPropertyChanged(BR.redeemInfo);
        notifyPropertyChanged(BR.actionText);
    }

    @Bindable
    public boolean isShowProgress() {
        return showProgress;
    }

    @Bindable
    public boolean isShowImageProgress() {
        return showImageProgress;
    }

    @Bindable
    public String getRedeemInfo() {
        return !isLoggedIn
            ? "MUST BE LOGGED IN TO REDEEM"
            : redeemingDate == null
            ? ""
            : "This coupon was redeemed on " + DATE_FORMAT.format(redeemingDate);
    }

    @Bindable
    public String getActionText() {
        return !isLoggedIn
            ? "LOGIN"
            : redeemingDate == null
            ? "REDEEM"
            : "";
    }
}
