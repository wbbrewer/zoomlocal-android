package com.zoomlocal.listings.ui.vendor.details;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoomlocal.listings.R;
import com.zoomlocal.listings.databinding.FragmentVendorDetailTabBinding;
import com.zoomlocal.listings.model.Vendor;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link VendorDetailTabFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VendorDetailTabFragment extends Fragment {
    public static final String TAG = VendorDetailTabFragment.class.getSimpleName();

    private static final String ARG_MODEL = "arg.model";

    public static VendorDetailTabFragment newInstance(Vendor vendor) {
        VendorDetailTabFragment fragment = new VendorDetailTabFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_MODEL, vendor);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragmentVendorDetailTabBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_vendor_detail_tab, container, false);

        Vendor vendor = (Vendor) getArguments().getSerializable(ARG_MODEL);
        binding.setViewModel(new VendorDetailViewModel(vendor));
        binding.socialShareView.setVendor(vendor);
        return binding.getRoot();
    }
}
