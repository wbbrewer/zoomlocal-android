package com.zoomlocal.listings.ui.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoomlocal.listings.App;
import com.zoomlocal.listings.LocalDataManager;
import com.zoomlocal.listings.ui.BaseListFragment;
import com.zoomlocal.listings.util.Progressable;

/**
 *
 */

public abstract class BaseSearchFragment<T> extends BaseListFragment<T> {
    private String search = "";

    protected final LocalDataManager localDataManager = App.getInstance().getLocalDataManager();

    public BaseSearchFragment() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(RADIUS_CHOOSER_VISIBLE, false);
        bundle.putBoolean(ENDLESS_LOADER, false);
        bundle.putBoolean(SWIPE_ENABLED, false);
        setArguments(bundle);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        emptyOrErrorView.showCustom("ZoomLocal", "Find What You Want, When You Want It.");
        return view;
    }

    @Override
    protected void requestData(boolean fromBeginning, Progressable progressable) {
        if (search.length() >= 3) {
            super.requestData(fromBeginning, progressable);
        }
    }

    public void setSearch(String search) {
        this.search = search;
        update();
    }

    public String getSearch() {
        return search;
    }
}
