package com.zoomlocal.listings.ui.vendor.details;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.zoomlocal.listings.R;
import com.zoomlocal.listings.model.Vendor;
import com.zoomlocal.listings.util.Util;

/**
 * Created by aricbrown on 11/6/15.
 */
public class VendorDetailViewModel {
    private final static String TAG = VendorDetailViewModel.class.getSimpleName();
    public Vendor vendor;

    public VendorDetailViewModel(Vendor vendor) {
        this.vendor = vendor;
    }

    public String getName() {
        return vendor.getName();
    }

    public String getDescription() {
        return vendor.getDescription();
    }

    public String getImageUrl() {
        return vendor.getVendorMeta().getImages().getMedium().getUrl();
    }

    @BindingAdapter({"android:src"})
    public static void setImage(final ImageView view, String url) {
        Picasso.with(view.getContext())
            .load(url)
            .error(R.drawable.ic_shop_placeholder)
            .placeholder(R.drawable.ic_shop_placeholder)
            .into(view);
    }

    public String getVendorAddress() {
        return vendor.getVendorMeta().getAddress1();

    }

    public String getVendorCityState() {

        return vendor.getVendorMeta().getCity() + "," + vendor.getVendorMeta().getState();

    }

    public String getTelephone() {
        return vendor.getVendorMeta().getTelephone();
    }

    public String getSatHour() {
        String hour = "  -  ";
        String startHour = vendor.getVendorMeta().getHours().getSat().getOpenTime();
        String CloseHour = vendor.getVendorMeta().getHours().getSat().getCloseTime();

        if (!Util.isNullOrEmpty(startHour) & !Util.isNullOrEmpty(CloseHour)) {
            hour += startHour;
            hour += " to ";
            hour += CloseHour;

        }
        return hour;
    }

    public String getSunHour() {
        String hour = "  -  ";
        String startHour = vendor.getVendorMeta().getHours().getSun().getOpenTime();
        String CloseHour = vendor.getVendorMeta().getHours().getSun().getCloseTime();

        if (!Util.isNullOrEmpty(startHour) & !Util.isNullOrEmpty(CloseHour)) {
            hour += startHour;
            hour += " to ";
            hour += CloseHour;

        }
        return hour;
    }

    public String getMonHour() {
        String hour = "  -  ";
        String startHour = vendor.getVendorMeta().getHours().getMon().getOpenTime();
        String CloseHour = vendor.getVendorMeta().getHours().getMon().getCloseTime();
        ;

        if (!Util.isNullOrEmpty(startHour) & !Util.isNullOrEmpty(CloseHour)) {
            hour += startHour;
            hour += " to ";
            hour += CloseHour;

        }
        return hour;
    }

    public String getTueHour() {
        String hour = "  -  ";
        String startHour = vendor.getVendorMeta().getHours().getTue().getOpenTime();
        String CloseHour = vendor.getVendorMeta().getHours().getTue().getCloseTime();
        ;

        if (!Util.isNullOrEmpty(startHour) & !Util.isNullOrEmpty(CloseHour)) {
            hour += startHour;
            hour += " to ";
            hour += CloseHour;

        }
        return hour;
    }

    public String getWedHour() {
        String hour = "  -  ";
        String startHour = vendor.getVendorMeta().getHours().getWed().getOpenTime();
        String CloseHour = vendor.getVendorMeta().getHours().getWed().getCloseTime();
        ;

        if (!Util.isNullOrEmpty(startHour) & !Util.isNullOrEmpty(CloseHour)) {
            hour += startHour;
            hour += " to ";
            hour += CloseHour;

        }
        return hour;
    }

    public String getThuHour() {
        String hour = "  -  ";
        String startHour = vendor.getVendorMeta().getHours().getThu().getOpenTime();
        String CloseHour = vendor.getVendorMeta().getHours().getThu().getCloseTime();
        ;

        if (!Util.isNullOrEmpty(startHour) & !Util.isNullOrEmpty(CloseHour)) {
            hour += startHour;
            hour += " to ";
            hour += CloseHour;

        }
        return hour;
    }

    public String getFriHour() {
        String hour = "  -  ";
        String startHour = vendor.getVendorMeta().getHours().getFri().getOpenTime();
        String CloseHour = vendor.getVendorMeta().getHours().getFri().getCloseTime();
        ;

        if (!Util.isNullOrEmpty(startHour) & !Util.isNullOrEmpty(CloseHour)) {
            hour += startHour;
            hour += " to ";
            hour += CloseHour;

        }
        return hour;
    }
}
