package com.zoomlocal.listings.ui.vendor.details;


import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.MarkerOptions;
import com.zoomlocal.listings.model.Vendor;

public class VendorMapTabFragment extends SupportMapFragment implements OnMapReadyCallback {

    public static final String TAG = VendorMapTabFragment.class.getSimpleName();
    private static final String ARG_PARAM_MODEL = "model";

    private Vendor vendor;

    public static VendorMapTabFragment newInstance(Vendor vendor) {
        VendorMapTabFragment fragment = new VendorMapTabFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM_MODEL, vendor);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            vendor = (Vendor) getArguments().getSerializable(ARG_PARAM_MODEL);
        }

        getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.addMarker(new MarkerOptions()
            .position(vendor.getVendorMeta().getLatLon())
            .title(vendor.getName()));

        // Move the camera instantly to location with a zoom of 10.
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(vendor.getVendorMeta().getLatLon(), 10));

        // Zoom in, animating the camera.
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15), 1000, null);
    }
}
