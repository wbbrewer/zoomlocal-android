package com.zoomlocal.listings.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;

import com.zoomlocal.listings.App;
import com.zoomlocal.listings.R;

import java.io.Serializable;

/**
 */
public class FragmentHolderActivity extends BaseActivity {
    public static final String EXTRA_CONTENT_CLASS = "contentClass";
    public static final String EXTRA_CONTENT_ARGS = "contentArgs";
    public static final String EXTRA_TITLE = "title";

    private static final String FRAGMENT_CONTENT_TAG = "mainContent";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.app_bar_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String title = getIntent().getStringExtra(EXTRA_TITLE);
        if (!TextUtils.isEmpty(title)) {
            getSupportActionBar().setTitle(title);
        }

        if (savedInstanceState == null) {
            Fragment contentFragment = getFragmentFromExtras();
            if (contentFragment != null) {
                replaceContentFragment(contentFragment);
            }
        }
    }

    protected void replaceContentFragment(Fragment contentFragment) {
        replaceContentFragment(contentFragment, 0, 0);
    }

    protected void replaceContentFragment(Fragment fragment, int enterAnimation, int exitAnimation) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (enterAnimation > 0 && exitAnimation > 0) {
            transaction.setCustomAnimations(enterAnimation, exitAnimation);
        }
        transaction.replace(R.id.container, fragment, FRAGMENT_CONTENT_TAG);
        transaction.commit();
    }

    protected Fragment getContentFragment() {
        return getSupportFragmentManager().findFragmentByTag(FRAGMENT_CONTENT_TAG);
    }

    private Fragment getFragmentFromExtras() {
        Bundle extras = getIntent().getExtras();
        if (extras == null || !extras.containsKey(EXTRA_CONTENT_CLASS) || !extras.containsKey(EXTRA_CONTENT_ARGS)) {
            return null;
        }

        Fragment contentFragment = Fragment.instantiate(this, extras.getString(EXTRA_CONTENT_CLASS));
        contentFragment.setArguments(extras.getBundle(EXTRA_CONTENT_ARGS));
        return contentFragment;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public static class Builder {
        private final Class<? extends Fragment> clazz;
        private Context context;
        private Fragment fragment;

        private Intent intent;
        private int requestCode = Integer.MIN_VALUE;
        private Bundle bundle;

        public Builder(Context context, Class<? extends Fragment> clazz) {
            this.context = context;
            this.clazz = clazz;
            init();
        }

        public Builder(Fragment fragment, Class<? extends Fragment> clazz) {
            this.fragment = fragment;
            this.clazz = clazz;
            init();
        }

        private void init() {
            bundle = new Bundle();
            intent = new Intent(App.getInstance(), FragmentHolderActivity.class);
            intent.putExtra(FragmentHolderActivity.EXTRA_CONTENT_CLASS, clazz.getName());
        }

        public Builder bundle(Bundle bundle) {
            this.bundle.putAll(bundle);
            return this;
        }

        public Builder bundleString(String key, String value) {
            this.bundle.putString(key, value);
            return this;
        }

        public Builder bundleBoolean(String key, boolean value) {
            this.bundle.putBoolean(key, value);
            return this;
        }

        public Builder bundleParcelable(String key, Parcelable value) {
            this.bundle.putParcelable(key, value);
            return this;
        }

        public Builder bundleSerializable(String key, Serializable value) {
            this.bundle.putSerializable(key, value);
            return this;
        }

        public Builder putExtraBoolean(String key, boolean value) {
            intent.putExtra(key, value);
            return this;
        }

        public Builder title(String title) {
            intent.putExtra(FragmentHolderActivity.EXTRA_TITLE, title);
            return this;
        }

        public Builder requestCode(int requestCode) {
            this.requestCode = requestCode;
            return this;
        }

        public void buildAndStart() {
            intent.putExtra(FragmentHolderActivity.EXTRA_CONTENT_ARGS, bundle);

            if (context != null) {
                if (requestCode == Integer.MIN_VALUE) {
                    context.startActivity(intent);
                } else if (context instanceof Activity) {
                    ((Activity) context).startActivityForResult(intent, requestCode);
                } else {
                    throw new IllegalStateException("Cannot start for result non-activity context: " + context);
                }
            } else {
                if (requestCode == Integer.MIN_VALUE) {
                    fragment.startActivity(intent);
                } else {
                    fragment.startActivityForResult(intent, requestCode);
                }
            }
        }
    }
}
