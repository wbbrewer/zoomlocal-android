package com.zoomlocal.listings.ui.social;

import android.content.Context;
import android.net.Uri;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;
import com.zoomlocal.listings.App;
import com.zoomlocal.listings.R;
import com.zoomlocal.listings.model.Images;
import com.zoomlocal.listings.model.Product;
import com.zoomlocal.listings.model.Vendor;
import com.zoomlocal.listings.rest.ProgressedCallback;
import com.zoomlocal.listings.ui.BaseActivity;
import com.zoomlocal.listings.util.UiUtils;
import com.zoomlocal.listings.util.Util;
import com.zoomlocal.listings.util.ViewProgressable;

import java.io.File;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 *
 */
public class SocialShareView extends FrameLayout {
    private static final String TAG = "SocialShareView";

    private View twitterProgressView;
    private Vendor vendor;
    private Product product;

    public SocialShareView(Context context) {
        super(context);
        init();
    }

    public SocialShareView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SocialShareView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.view_social_share, this);
        twitterProgressView = findViewById(R.id.twitterProgressView);

        findViewById(R.id.twitterImageView).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onTwitterClick();
            }
        });
        findViewById(R.id.facebookImageView).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onFacebookClick();
            }
        });
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    private Images getImageToPost() {
        return vendor != null
            ? vendor.getVendorMeta().getImages()
            : product.getImages();
    }

    private String getTextToPost(boolean showLink) {
        return (vendor != null
            ? "Check out this retailer I found on ZoomLocal!"
            : "Hey, look what I found on ZoomLocal!")
            + (showLink ? "\n" + getLinkToPost() : "");
    }

    private String getLinkToPost() {
        return vendor != null
            ? vendor.getLink()
            : product.getLink();
    }

    private void onTwitterClick() {
        if (getContext().getExternalCacheDir() == null) {
            postInTwitter(getContext(), null);
            return;
        }

        final File imageFile = new File(getContext().getExternalCacheDir(), "downloading_image.png");
        Call<ResponseBody> downloadCall = App.getInstance().getAPIService().downloadFile(getImageToPost().getMedium().getUrl());
        downloadCall.enqueue(new ProgressedCallback<ResponseBody>(getContext(), new ViewProgressable(twitterProgressView)) {
            @Override
            protected void onSuccess(Call<ResponseBody> call, Response<ResponseBody> response) {
                boolean writtenToDisk = Util.writeResponseBodyToDisk(imageFile, response.body());

                postInTwitter(getContext(), writtenToDisk ? imageFile : null);
                UiUtils.setVisibility(false, twitterProgressView);
            }

            @Override
            protected void onServerError(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.d(TAG, "onServerError " + response.message());
                postInTwitter(getContext(), null);
            }

            @Override
            protected void onFinish(Call<ResponseBody> call, boolean success) {
                if (!success) {
                    super.onFinish(call, success);
                }
            }
        });
    }

    private void postInTwitter(Context context, File file) {
        TweetComposer.Builder builder = new TweetComposer.Builder(context)
            .text(getTextToPost(true));
        if (file != null) {
            builder.image(Uri.fromFile(file));
        }

        builder.show();
    }

    private void onFacebookClick() {
        final BaseActivity baseActivity = (BaseActivity) getContext();
        final ShareDialog shareDialog = new ShareDialog(baseActivity);

        shareDialog.registerCallback(baseActivity.getCallbackManager(), new FacebookCallback<Sharer.Result>() {

            @Override
            public void onSuccess(Sharer.Result result) {
                Log.d(TAG, "success");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "error", error);
                Toast.makeText(baseActivity, "Error during sharing to Facebook: " + error.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "cancel");
            }
        });

        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                .setContentTitle("ZoomLocal")
                .setContentDescription(getTextToPost(false))
                .setContentUrl(Uri.parse(getLinkToPost()))
                .setImageUrl(Uri.parse(getImageToPost().getMedium().getUrl()))
                .build();

            shareDialog.show(linkContent);
        }
    }
}
