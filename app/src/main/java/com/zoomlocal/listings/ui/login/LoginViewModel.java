package com.zoomlocal.listings.ui.login;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import com.zoomlocal.listings.BR;
import com.zoomlocal.listings.App;
import com.zoomlocal.listings.BuildConfig;
import com.zoomlocal.listings.R;
import com.zoomlocal.listings.model.LoginInfo;
import com.zoomlocal.listings.rest.UserService;
import com.zoomlocal.listings.util.SimpleTextWatcher;
import com.zoomlocal.listings.util.Util;

import retrofit2.Call;
import retrofit2.Response;


/**
 *
 */

public class LoginViewModel extends BaseObservable {
    private LoginSignUpViewModel loginSignUpViewModel;

    private String email;
    private String password;

    public LoginViewModel(LoginSignUpViewModel loginSignUpViewModel) {
        this.loginSignUpViewModel = loginSignUpViewModel;

        if (BuildConfig.DEBUG) {
            email = "eklishevich1234@gmail.com";
            password = "1qwaszx";
        }
    }

    @Bindable
    public String getEmailError() {
        if (TextUtils.isEmpty(email)) {
            return App.getResourcesStatic().getString(R.string.error_email);
        } else if (!Util.isEmailValid(email)) {
            return App.getResourcesStatic().getString(R.string.error_email_valid);
        }
        return null;
    }

    @Bindable
    public String getPasswordError() {
        if (TextUtils.isEmpty(password)) {
            return App.getResourcesStatic().getString(R.string.error_password);
        }
        return null;
    }

    public TextWatcher getValidationWatcher() {
        return new SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
                notifyPropertyChanged(BR.emailError);
                notifyPropertyChanged(BR.passwordError);
            }
        };
    }

    public void onLoginClicked(final View view) {
        if (getEmailError() == null) {
            Call<LoginInfo> loginCall = App.getInstance().getUserService().login("password",
                UserService.CLIENT_ID, UserService.CLIENT_SECRET, email, password);
            loginCall.enqueue(new LoginCallbackHandler(view.getContext(), loginSignUpViewModel, email) {
                @Override
                protected void onSuccess(Call<LoginInfo> call, Response<LoginInfo> response) {
                    showToast("Login succeeded");
                    super.onSuccess(call, response);
                }
            });
        }
    }

    public void onSignUpClicked(View view) {
        loginSignUpViewModel.updateCurrentScreen(LoginSignUpViewModel.State.SIGN_UP);
    }

    public void onResetPasswordClicked(View view) {
        loginSignUpViewModel.updateCurrentScreen(LoginSignUpViewModel.State.RESET);
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
