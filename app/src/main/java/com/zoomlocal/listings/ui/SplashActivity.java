package com.zoomlocal.listings.ui;

import android.content.Intent;
import android.os.Bundle;

import com.zoomlocal.listings.App;
import com.zoomlocal.listings.AdAfterSplashDownloader;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AdAfterSplashDownloader adAfterSplashDownloader = App.getInstance().getAdAfterSplashDownloader();
        adAfterSplashDownloader.updateAdAfterSplashBitmap(this, new AdAfterSplashDownloader.UpdateCallback() {
            @Override
            public void onSuccess() {
                startMainActivity();
                finish();
            }

            @Override
            public void onFailure() {
                startMainActivity();
                finish();
            }
        });
    }

    private void startMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
