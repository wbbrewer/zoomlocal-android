package com.zoomlocal.listings.ui.search;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 *
 */

public class SearchPagerAdapter extends FragmentStatePagerAdapter {
    public SearchPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ListingSearchFragment();
            case 1:
                return new EventsSearchFragment();
            case 2:
                return new VendorsSearchFragment();
            default:
                throw new IllegalArgumentException("No page for position: " + position);
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Listing";
            case 1:
                return "Events";
            case 2:
                return "Retailers";
            default:
                throw new IllegalArgumentException("No page for position: " + position);
        }
    }
}
