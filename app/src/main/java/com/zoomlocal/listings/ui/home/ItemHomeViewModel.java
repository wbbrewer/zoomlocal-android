package com.zoomlocal.listings.ui.home;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

/**
 *
 */

public class ItemHomeViewModel {
    private HomeItem homeItem;
    public boolean rightDelimeterVisible;
    public boolean bottomDelimeterVisible;

    @BindingAdapter({"android:src"})
    public static void setImageViewResource(ImageView imageView, int resource) {
        imageView.setImageResource(resource);
    }

    public ItemHomeViewModel(HomeItem homeItem, boolean rightDelimeterVisible, boolean bottomDelimeterVisible) {
        this.homeItem = homeItem;
        this.rightDelimeterVisible = rightDelimeterVisible;
        this.bottomDelimeterVisible = bottomDelimeterVisible;
    }

    public int getCategoryImage() {
        return homeItem.getIcon();
    }

    public String getCategoryName() {
        return homeItem.getName();
    }
}
