package com.zoomlocal.listings.ui.search;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.zoomlocal.listings.R;
import com.zoomlocal.listings.ui.BaseActivity;

import java.util.List;

/**
 *
 */

public class SearchActivity extends BaseActivity {
    private SearchPagerAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Search");

        EditText searchEditText = (EditText) findViewById(R.id.searchEditText);
        searchEditText.setOnEditorActionListener(new SearchHandler());

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        adapter = new SearchPagerAdapter(getSupportFragmentManager());
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(3);

        //Notice how The Tab Layout adn View Pager object are linked
        tabLayout.setupWithViewPager(viewPager);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
    }

    private void performSearch(String search) {
        if (search.length() >= 3) {
            List<Fragment> fragments = getSupportFragmentManager().getFragments();
            for (int i = 0; fragments != null && i < fragments.size(); i++) {
                if (fragments.get(i) instanceof BaseSearchFragment) {
                    ((BaseSearchFragment) fragments.get(i)).setSearch(search);
                }
            }
        }
    }

    private class SearchHandler implements TextView.OnEditorActionListener {
        @Override
        public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                performSearch(textView.getText().toString());
                return true;
            }
            return false;
        }
    }
}
