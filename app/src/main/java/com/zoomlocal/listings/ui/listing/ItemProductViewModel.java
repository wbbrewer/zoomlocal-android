package com.zoomlocal.listings.ui.listing;

import android.util.Log;

import com.zoomlocal.listings.App;
import com.zoomlocal.listings.LocalDataManager;
import com.zoomlocal.listings.model.Product;
import com.zoomlocal.listings.util.Util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by aricbrown on 1/7/16.
 */
public class ItemProductViewModel {

    public Product product;

    private LocalDataManager localDataManager = App.getInstance().getLocalDataManager();

    public ItemProductViewModel(Product product) {
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }

    public Boolean isEvent() {
        if (product != null && product.getMeta() != null && product.getMeta().getRqEventStartDate() != null) {
            if (product.getMeta().getRqEventStartDate().length() > 0) {
                // event
                return true;
            }
        }
        return false;
    }

    public String getTagLine() {
        if (isEvent()) {
            // event
            if (product != null && product.getMeta() != null && product.getMeta().getRqEventEndDate() != null) {
                if (product.getMeta().getRqEventEndDate().length() > 0) {
                    return String.format("%s - %s", formatDateFromString(product.getMeta().getRqEventStartDate()), formatDateFromString(product.getMeta().getRqEventEndDate()));
                }
            }
            return formatDateFromString(product.getMeta().getRqEventStartDate());
        } else {
            return "";
        }
    }

    public String getTitle() {
        return product.getTitle();
    }

    public String getVendorName() {

        return product.getTerms().getVendor().getVendorMeta().getName();
    }

    public String getFooterText() {
        return product.getTerms().getVendor().getVendorMeta().getCity() + "," + product.getTerms().getVendor().getVendorMeta().getState();
    }

    public String getImage() {
        return product.getImages().getThumbnail().getUrl();
    }

    public String getPrice() {

        return product.getMeta().getPrice();
    }

    public String formatDateFromString(String dtStart) {
        Date date = null;
        //String dtStart = "2010-10-15T09:27:37Z";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        try {
            date = format.parse(dtStart);
        } catch (ParseException e) {
            Log.d("test", "cannot parse date: " + dtStart, e);
        }
        return DateFormat.getDateInstance(DateFormat.SHORT).format(date);
    }

    public String getDistanceFromMe() {
        float distanceMeters = localDataManager.getLocation().distanceTo(product.getTerms().getVendor().getVendorMeta().getGeoloc().getLocation());
        return String.format(Locale.ENGLISH, "%1$.1f miles", Util.metersToMiles(distanceMeters));
    }

    //todo: implement like button ZLItemDetailsViewController
}
