package com.zoomlocal.listings.ui.radius;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.zoomlocal.listings.App;
import com.zoomlocal.listings.LocalDataManager;
import com.zoomlocal.listings.R;
import com.zoomlocal.listings.ui.FragmentHolderActivity;

/**
 */
public class RadiusView extends FrameLayout implements View.OnClickListener {

    private LocalDataManager localDataManager;

    private TextView milesTextView;

    public RadiusView(Context context) {
        super(context);
        init();
    }

    public RadiusView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RadiusView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            localDataManager = App.getInstance().getLocalDataManager();
        }

        LayoutInflater.from(getContext()).inflate(R.layout.view_radius, this);
        milesTextView = (TextView) findViewById(R.id.milesTextView);

        updateRadius();

        setOnClickListener(this);
    }

    private void updateRadius() {
        milesTextView.setText(localDataManager.getRadiusMiles() + "mi");
    }

    @Override
    protected void onWindowVisibilityChanged(int visibility) {
        super.onWindowVisibilityChanged(visibility);

        if (visibility == View.VISIBLE) {
            updateRadius();
        }
    }

    @Override
    public void onClick(View view) {
        new FragmentHolderActivity.Builder(view.getContext(), RadiusChooserFragment.class)
            .title("Choose distance")
            .buildAndStart();
    }
}
