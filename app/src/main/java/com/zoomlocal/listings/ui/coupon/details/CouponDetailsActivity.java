package com.zoomlocal.listings.ui.coupon.details;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;

import com.zoomlocal.listings.App;
import com.zoomlocal.listings.R;
import com.zoomlocal.listings.UserData;
import com.zoomlocal.listings.databinding.LayoutCouponDetailsBinding;
import com.zoomlocal.listings.model.Coupon;
import com.zoomlocal.listings.rest.ProgressedCallback;
import com.zoomlocal.listings.ui.BaseActivity;

import retrofit2.Call;
import retrofit2.Response;

/**
 *
 */

public class CouponDetailsActivity extends BaseActivity {
    public static final String COUPON = "COUPON";

    private Coupon coupon;

    private UserData userData = App.getInstance().getUserData();
    private CouponDetailsViewModel viewModel;

    private UserData.OnLoginListener loginHandler = new LoginHandler();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LayoutCouponDetailsBinding viewDataBinding = DataBindingUtil.setContentView(this, R.layout.layout_coupon_details);

        coupon = (Coupon) getIntent().getSerializableExtra(COUPON);

        setSupportActionBar(viewDataBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(coupon.getTitle());
        getSupportActionBar().setSubtitle(coupon.getTerms().getVendor().getVendorMeta().getName());

        viewModel = new CouponDetailsViewModel(coupon.getImages().getLarge().getUrl(), coupon.getRedeemingDate(), this, viewDataBinding.imageView);
        viewDataBinding.setViewModel(viewModel);

        viewModel.setLoggedIn(userData.isLoggedIn());
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (userData.isLoggedIn()) {
            sendUpdateDetailsRequest();
        }
        userData.addListener(loginHandler);
    }

    @Override
    protected void onStop() {
        super.onStop();
        userData.removeListener(loginHandler);
    }

    private void sendUpdateDetailsRequest() {
        Call<Coupon> couponCall = App.getInstance().getAPIService().getCoupon(this.coupon.getId(), userData.getAccessToken());
        couponCall.enqueue(new ProgressedCallback<Coupon>(this, viewModel) {
            @Override
            protected void onSuccess(Call<Coupon> call, Response<Coupon> response) {
                viewModel.setRedeemingDate(response.body().getRedeemingDate());
            }
        });
    }

    public void sendRedeemRequest() {
        Call<Coupon> couponCall = App.getInstance().getAPIService().redeemCoupon(coupon.getId(), userData.getAccessToken(), 1);
        couponCall.enqueue(new ProgressedCallback<Coupon>(this, viewModel) {
            @Override
            protected void onSuccess(Call<Coupon> call, Response<Coupon> response) {
                viewModel.setRedeemingDate(response.body().getRedeemingDate());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private class LoginHandler implements UserData.OnLoginListener {

        @Override
        public void onLoginStateChanged(boolean isLoggedIn) {
            viewModel.setLoggedIn(isLoggedIn);
            sendUpdateDetailsRequest();
        }
    }
}
