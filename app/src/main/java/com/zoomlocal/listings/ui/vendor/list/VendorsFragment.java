package com.zoomlocal.listings.ui.vendor.list;


import android.location.Location;
import android.support.v7.widget.RecyclerView;

import com.zoomlocal.listings.App;
import com.zoomlocal.listings.model.Vendor;
import com.zoomlocal.listings.ui.BaseListFragment;
import com.zoomlocal.listings.ui.PaginationAdapter;

import java.util.List;

import retrofit2.Call;

/**
 * Created by aricbrown on 10/29/15.
 */
public class VendorsFragment extends BaseListFragment<Vendor> {
    public static final String TAG = "VendorsFragment";

    public static final String CATEGORY = "CATEGORY";

    @Override
    protected PaginationAdapter<Vendor, ? extends RecyclerView.ViewHolder> newPaginationAdapter() {
        return new VendorsAdapter();
    }

    @Override
    protected Call<List<Vendor>> newCall() {
        Location location = localDataManager.getLocation();
        return App.getInstance().getAPIService().getVendors(location.getLatitude(), location.getLongitude(),
            getCurrentPage(), 10, localDataManager.getRadiusMiles(), getArgumentsNonNull().getString(CATEGORY));
    }
}
