package com.zoomlocal.listings.ui.vendor.list;

import android.content.Intent;
import android.databinding.BindingAdapter;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.zoomlocal.listings.App;
import com.zoomlocal.listings.LocalDataManager;
import com.zoomlocal.listings.R;
import com.zoomlocal.listings.model.Vendor;
import com.zoomlocal.listings.ui.vendor.details.VendorActivity;
import com.zoomlocal.listings.util.Util;

import java.util.Locale;

/**
 * Created by aricbrown on 11/6/15.
 */
public class ItemVendorViewModel {
    public Vendor vendor;

    private LocalDataManager localDataManager = App.getInstance().getLocalDataManager();

    public ItemVendorViewModel(Vendor vendor) {
        this.vendor = vendor;
    }

    @BindingAdapter({"android:src"})
    public static void setImage(final ImageView view, String url) {
        Picasso.with(view.getContext())
            .load(url)
            .error(R.drawable.ic_shop_placeholder)
            .placeholder(R.drawable.ic_shop_placeholder)
            .into(view);
    }

    public String getImage() {
        return vendor.getVendorMeta().getImages().getMedium().getUrl();
    }

    public String getName() {
        return vendor.getName();
    }

    public String getAddress() {
        return vendor.getVendorMeta().getAddress1();
    }

    public String getCityState() {
        return vendor.getCityState();
    }

    public String getDistanceFromMe() {
        float distanceMeters = localDataManager.getLocation().distanceTo(vendor.getVendorMeta().getGeoloc().getLocation());
        return String.format(Locale.ENGLISH, "%1$.1f miles", Util.metersToMiles(distanceMeters));
    }

    public View.OnClickListener onClickItem() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detailIntent = new Intent(v.getContext(), VendorActivity.class);
                detailIntent.putExtra(VendorActivity.EXTRA_MODEL, vendor);
                v.getContext().startActivity(detailIntent);
            }
        };
    }
}
