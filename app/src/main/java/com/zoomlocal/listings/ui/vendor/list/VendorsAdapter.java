package com.zoomlocal.listings.ui.vendor.list;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.zoomlocal.listings.R;
import com.zoomlocal.listings.databinding.ItemVendorBinding;
import com.zoomlocal.listings.model.Vendor;
import com.zoomlocal.listings.ui.PaginationAdapter;

/**
 * Created by aricbrown on 11/9/15.
 */
public class VendorsAdapter extends PaginationAdapter<Vendor, VendorsAdapter.BindingHolder> {


    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ItemVendorBinding binding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.getContext()),
            R.layout.item_vendor,
            parent,
            false);
        return new BindingHolder(binding);
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        holder.binding.setViewModel(new ItemVendorViewModel(getItem(position)));
    }

    public static class BindingHolder extends RecyclerView.ViewHolder {
        private ItemVendorBinding binding;

        public BindingHolder(ItemVendorBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }
}
