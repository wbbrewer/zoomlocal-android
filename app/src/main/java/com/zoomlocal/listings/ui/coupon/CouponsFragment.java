package com.zoomlocal.listings.ui.coupon;

import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;

import com.zoomlocal.listings.App;
import com.zoomlocal.listings.UserData;
import com.zoomlocal.listings.model.Coupon;
import com.zoomlocal.listings.ui.BaseListFragment;
import com.zoomlocal.listings.ui.PaginationAdapter;

import java.util.List;

import retrofit2.Call;

/**
 *
 */

public class CouponsFragment extends BaseListFragment<Coupon> {
    private static final String VENDOR_SLUG = "VENDOR_SLUG";

    private final UserData userData = App.getInstance().getUserData();

    private boolean dataLoadedForLoggedInState = userData.isLoggedIn();

    public static CouponsFragment newInstance(String vendorSlug) {
        CouponsFragment fragment = new CouponsFragment();
        Bundle args = new Bundle();
        args.putSerializable(VENDOR_SLUG, vendorSlug);
        args.putBoolean(RADIUS_CHOOSER_VISIBLE, false);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected boolean reloadOnResume() {
        return super.reloadOnResume() || dataLoadedForLoggedInState != userData.isLoggedIn();
    }

    @Override
    protected PaginationAdapter<Coupon, ? extends RecyclerView.ViewHolder> newPaginationAdapter() {
        return new CouponsAdapter();
    }

    @Override
    protected Call<List<Coupon>> newCall() {
        String vendorSlug = getArgumentsNonNull().getString(VENDOR_SLUG);
        dataLoadedForLoggedInState = userData.isLoggedIn();

        Call<List<Coupon>> couponsCall;
        if (TextUtils.isEmpty(vendorSlug)) {
            Location location = localDataManager.getLocation();
            int radius = localDataManager.getRadiusMiles();
            couponsCall = apiService.getCoupons(userData.getAccessTokenOrNull(), getCurrentPage(), location.getLatitude(), location.getLongitude(), radius);
        } else {
            couponsCall = apiService.getCouponsForVendor(userData.getAccessTokenOrNull(), vendorSlug, getCurrentPage());
        }

        return couponsCall;
    }
}
