package com.zoomlocal.listings.ui;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.zoomlocal.listings.App;
import com.zoomlocal.listings.LocalDataManager;
import com.zoomlocal.listings.R;
import com.zoomlocal.listings.rest.APIService;
import com.zoomlocal.listings.rest.ProgressedCallback;
import com.zoomlocal.listings.shared.EndlessRecyclerOnScrollListener;
import com.zoomlocal.listings.util.EmptyOrErrorView;
import com.zoomlocal.listings.util.Progressable;
import com.zoomlocal.listings.util.SwipeProgressable;
import com.zoomlocal.listings.util.UiUtils;
import com.zoomlocal.listings.util.Util;
import com.zoomlocal.listings.util.ViewProgressable;

import java.util.List;

import io.nlopez.smartlocation.OnLocationUpdatedListener;
import io.nlopez.smartlocation.SmartLocation;
import retrofit2.Call;
import retrofit2.Response;

/**
 */
public abstract class BaseListFragment<T> extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    public static final String RADIUS_CHOOSER_VISIBLE = "RADIUS_CHOOSER_VISIBLE";
    public static final String ENDLESS_LOADER = "ENDLESS_LOADER";
    public static final String SWIPE_ENABLED = "SWIPE_ENABLED";

    protected final LocalDataManager localDataManager = App.getInstance().getLocalDataManager();
    protected final APIService apiService = App.getInstance().getAPIService();

    private SwipeRefreshLayout swipeRefresh;
    private PaginationAdapter<T, ? extends RecyclerView.ViewHolder> adapter;
    private RecyclerView recyclerView;
    private View progressView;

    protected EmptyOrErrorView emptyOrErrorView;

    private LocalDataManager.Radius dataLoadedForRadius = localDataManager.getRadius();
    private Location dataLoadedForLocation = localDataManager.getLocation();
    private int currentPage = 1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_deals, container, false);

        swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipeRefresh);
        emptyOrErrorView = (EmptyOrErrorView) view.findViewById(R.id.emptyOrErrorView);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        progressView = view.findViewById(R.id.progressView);

        UiUtils.setVisibility(getArgumentsNonNull().getBoolean(RADIUS_CHOOSER_VISIBLE, true), view.findViewById(R.id.radiusView));

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        adapter = newPaginationAdapter();
        recyclerView.setAdapter(adapter);

        if (getArgumentsNonNull().getBoolean(ENDLESS_LOADER, true)) {
            recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
                @Override
                public void onLoadMore() {
                    requestData(false, new SwipeProgressable(swipeRefresh));
                }
            });
        }

        swipeRefresh.setOnRefreshListener(this);
        swipeRefresh.setEnabled(getArgumentsNonNull().getBoolean(SWIPE_ENABLED, true));

        update();

        if (Util.locationPermissionGranted(view.getContext())) {
            SmartLocation.with(getActivity()).location()
                .oneFix()
                .start(new OnLocationUpdatedListener() {
                    @Override
                    public void onLocationUpdated(Location location) {
                        if (dataLoadedForLocation.distanceTo(localDataManager.getLocation()) > 500) {
                            requestData(true, new ViewProgressable(progressView));
                        }
                    }
                });
        }

        return view;
    }

    @Override
    public void onRefresh() {
        requestData(true, new SwipeProgressable(swipeRefresh));
    }

    @Override
    public void onResume() {
        super.onResume();

        if (reloadOnResume()) {
            requestData(true, new ViewProgressable(progressView));
        }
    }

    protected boolean reloadOnResume() {
        return dataLoadedForRadius != localDataManager.getRadius();
    }

    protected abstract PaginationAdapter<T, ? extends RecyclerView.ViewHolder> newPaginationAdapter();

    protected abstract Call<List<T>> newCall();

    protected void update() {
        requestData(true, new ViewProgressable(progressView));
    }

    public int getCurrentPage() {
        return currentPage;
    }

    protected void requestData(boolean fromBeginning, Progressable progressable) {
        currentPage = fromBeginning ? 1 : currentPage;

        dataLoadedForRadius = localDataManager.getRadius();
        dataLoadedForLocation = localDataManager.getLocation();

        Call<List<T>> couponsCall = newCall();
        couponsCall.enqueue(new ResponseHandler(getActivity(), progressable, fromBeginning));
    }

    protected Bundle getArgumentsNonNull() {
        return getArguments() == null ? new Bundle() : getArguments();
    }

    private class ResponseHandler extends ProgressedCallback<List<T>> {
        private boolean fromBeginning;

        private ResponseHandler(Context context, Progressable progressable, boolean fromBeginning) {
            super(context, progressable);
            this.fromBeginning = fromBeginning;
        }

        @Override
        protected void onSuccess(Call<List<T>> call, Response<List<T>> response) {
            currentPage++;
            if (fromBeginning) {
                adapter.setItems(response.body());

                emptyOrErrorView.showEmptyView(response.body().isEmpty());
            } else {
                emptyOrErrorView.hide();
                adapter.addItems(response.body());
            }
        }

        protected void onServerError(Call<List<T>> call, Response<List<T>> response) {
            if (fromBeginning && adapter.getItemCount() == 0) {
                emptyOrErrorView.showServerError(response);
            } else {
                super.onServerError(call, response);
            }
        }

        protected void onNetworkError(Call<List<T>> call, Throwable t) {
            if (fromBeginning && adapter.getItemCount() == 0) {
                emptyOrErrorView.showNetworkError(t);
            } else {
                super.onNetworkError(call, t);
            }
        }
    }
}
