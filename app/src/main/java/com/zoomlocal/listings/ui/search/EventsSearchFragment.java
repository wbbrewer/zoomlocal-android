package com.zoomlocal.listings.ui.search;

import android.support.v7.widget.RecyclerView;

import com.zoomlocal.listings.model.Product;
import com.zoomlocal.listings.ui.PaginationAdapter;
import com.zoomlocal.listings.ui.listing.ProductAdapter;

import java.util.List;

import retrofit2.Call;

/**
 *
 */

public class EventsSearchFragment extends BaseSearchFragment<Product> {


    @Override
    protected PaginationAdapter<Product, ? extends RecyclerView.ViewHolder> newPaginationAdapter() {
        return new ProductAdapter();
    }

    @Override
    protected Call<List<Product>> newCall() {
        return apiService.searchEvents(getSearch());
    }
}
