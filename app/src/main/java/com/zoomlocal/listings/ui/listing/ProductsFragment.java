package com.zoomlocal.listings.ui.listing;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;

import com.zoomlocal.listings.App;
import com.zoomlocal.listings.UserData;
import com.zoomlocal.listings.model.Product;
import com.zoomlocal.listings.ui.BaseListFragment;
import com.zoomlocal.listings.ui.PaginationAdapter;
import com.zoomlocal.listings.util.ProductType;

import java.util.List;

import retrofit2.Call;

/**
 */
public class ProductsFragment extends BaseListFragment<Product> {
    public static final String PRODUCT_TYPE = "PRODUCT_TYPE";
    public static final String SLUG = "SLUG";

    private UserData userData = App.getInstance().getUserData();

    public static ProductsFragment newInstance(ProductType productType) {
        return newInstance(productType, null, true);
    }

    public static ProductsFragment newInstance(ProductType productType, String slug, boolean radiusVisible) {
        Bundle bundle = new Bundle();
        bundle.putInt(PRODUCT_TYPE, productType.ordinal());
        bundle.putString(SLUG, slug);
        bundle.putBoolean(RADIUS_CHOOSER_VISIBLE, radiusVisible);
        ProductsFragment fragment = new ProductsFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected PaginationAdapter<Product, ? extends RecyclerView.ViewHolder> newPaginationAdapter() {
        return new ProductAdapter();
    }

    @Override
    protected Call<List<Product>> newCall() {
        ProductType productType = ProductType.values()[getArgumentsNonNull().getInt(PRODUCT_TYPE)];
        String slug = getArgumentsNonNull().getString(SLUG);

        switch (productType) {
            case LISTING:
                return apiService.getListings(
                        userData.getAccessTokenOrNull(),
                        getCurrentPage(),
                        localDataManager.getLocation().getLatitude(),
                        localDataManager.getLocation().getLongitude(),
                        localDataManager.getRadiusMiles(),
                        slug
                );
            case EVENT:
                return apiService.getEvents(
                        getCurrentPage(),
                        localDataManager.getLocation().getLatitude(),
                        localDataManager.getLocation().getLongitude(),
                        localDataManager.getRadiusMiles()
                );
            case ALL:
                return apiService.getAllProducts(slug,
                        getCurrentPage()
                );
            default:
                throw new IllegalArgumentException("Unknown product type: " + productType);
        }
    }
}
