package com.zoomlocal.listings.ui.login;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.zoomlocal.listings.App;
import com.zoomlocal.listings.BR;
import com.zoomlocal.listings.BuildConfig;
import com.zoomlocal.listings.R;
import com.zoomlocal.listings.model.SignUpError;
import com.zoomlocal.listings.rest.ProgressedCallback;
import com.zoomlocal.listings.rest.UserService;
import com.zoomlocal.listings.util.SimpleTextWatcher;
import com.zoomlocal.listings.util.Util;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;


/**
 *
 */

public class ResetViewModel extends BaseObservable {

    private UserService userService = App.getInstance().getUserService();

    private String email;
    private LoginSignUpViewModel loginSignUpViewModel;

    public ResetViewModel(LoginSignUpViewModel loginSignUpViewModel) {
        this.loginSignUpViewModel = loginSignUpViewModel;

        if (BuildConfig.DEBUG) {
            email = "eklishevich1234@gmail.com";
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Bindable
    public String getEmailError() {
        if (TextUtils.isEmpty(email)) {
            return App.getResourcesStatic().getString(R.string.error_email);
        } else if (!Util.isEmailValid(email)) {
            return App.getResourcesStatic().getString(R.string.error_email_valid);
        }
        return null;
    }

    public TextWatcher getValidationWatcher() {
        return new SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                super.onTextChanged(s, start, before, count);
                notifyPropertyChanged(BR.emailError);
            }
        };
    }

    public void onResetPasswordClicked(View view) {
        if (TextUtils.isEmpty(getEmailError())) {
            Call<ResponseBody> resetCall = userService.resetPassword(email);
            resetCall.enqueue(new ProgressedCallback<ResponseBody>(view.getContext(), loginSignUpViewModel) {

                @Override
                protected void onSuccess(Call<ResponseBody> call, Response<ResponseBody> response) {
                    showToast("An email has been sent to " + email);

                    loginSignUpViewModel.updateCurrentScreen(LoginSignUpViewModel.State.LOGIN);
                }

                @Override
                protected void onServerError(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        SignUpError error = App.getInstance().getGson().fromJson(response.errorBody().string(), SignUpError.class);
                        showToast(error.getErrorDescription());
                    } catch (Exception e) {
                        Log.d("test", "Error during parsing error", e);
                        super.onServerError(call, response);
                    }
                }
            });
        }
    }
}
