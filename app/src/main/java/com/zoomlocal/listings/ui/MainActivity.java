package com.zoomlocal.listings.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;

import com.zoomlocal.listings.App;
import com.zoomlocal.listings.AdAfterSplashDownloader;
import com.zoomlocal.listings.R;
import com.zoomlocal.listings.UserData;
import com.zoomlocal.listings.rest.SilentCallback;
import com.zoomlocal.listings.rest.UserService;
import com.zoomlocal.listings.ui.coupon.CouponsFragment;
import com.zoomlocal.listings.ui.home.HomeFragment;
import com.zoomlocal.listings.ui.listing.ProductsFragment;
import com.zoomlocal.listings.ui.login.LoginSignUpDialogFragment;
import com.zoomlocal.listings.ui.vendor.VendorsOnMapFragment;
import com.zoomlocal.listings.util.ProductType;

import org.mobiledeeplinking.android.MobileDeepLinking;

import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, UserData.OnLoginListener {

    private static final String DEEP_LINKING_DEALS_ROUTE = "deals";
    private static final String DEEP_LINKING_LISTINGS_ROUTE = "listings";
    private static final String DEEP_LINKING_EVENTS_ROUTE = "events";
    // Under "root" is understood part without route paremeter.
    // Mapping is defined by MobileDeepLinking.json file.
    private static final Map<String, String> DEEPLINKING_HANDLER_TO_ROUTE_MAP = new HashMap<String, String>(){{
        put( "goToDeals", DEEP_LINKING_DEALS_ROUTE);
        put( "goToSpecificDeal", DEEP_LINKING_DEALS_ROUTE);
        put( "goToListings", DEEP_LINKING_LISTINGS_ROUTE);
        put( "goToSpecificListing", DEEP_LINKING_LISTINGS_ROUTE);
        put( "goToEvents", DEEP_LINKING_EVENTS_ROUTE);
        put( "goToSpecificEvent", DEEP_LINKING_EVENTS_ROUTE);
    }};

    private static final String TAG = "MainActivity";

    private static final String STATE_TITLE = "STATE_TITLE";
    private static final String EXTRA_DEEP_LINKING_ROUTE = "com.zoomlocal.listings.ui.deep_linking_route";

    private NavigationView navigationView;

    private UserData userData = App.getInstance().getUserData();
    private UserService userService = App.getInstance().getUserService();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {
            selectNavigationItem(R.id.nav_home);
        } else {
            getSupportActionBar().setTitle(savedInstanceState.getString(STATE_TITLE));
        }

        startAdAfterSplashActivityIfNeeded();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        String deepLinkingRoute = deepLinkingRoute(intent);
        if (deepLinkingRoute != null) {
            String itemId = intent.getExtras().getString("id");

            if (deepLinkingRoute.equals(DEEP_LINKING_DEALS_ROUTE)) {
                if (itemId == null) {
                    selectNavigationItem(R.id.nav_deals);
                } else {

                }
            }
            else if (deepLinkingRoute.equals(DEEP_LINKING_LISTINGS_ROUTE)) {
                if (itemId == null) {
                    selectNavigationItem(R.id.nav_listings);
                } else {

                }
            }
            else if (deepLinkingRoute.equals(DEEP_LINKING_EVENTS_ROUTE)) {
                if (itemId == null) {
                    selectNavigationItem(R.id.nav_events);
                } else {

                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        updateLoginState();
        userData.addListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        userData.addListener(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (outState != null && !TextUtils.isEmpty(getSupportActionBar().getTitle())) {
            outState.putString(STATE_TITLE, getSupportActionBar().getTitle().toString());
        }

        super.onSaveInstanceState(outState);
    }

    /**
     * TODO: Incapsulate details of deep linking into separate inner class.
     * If the class of activity is not specified in configuration json file and the default activity
     * class is not provided then app will crash.
     *
     * If a class of activity is not specified for a specific route then library will try to
     * route to default route ignoring route paremeters including those that where added in handler
     * for initial route.
     */
    public static void setUpMobileDeeplinkingSDK(final Context contextForIntent) {

        for (final String handler : DEEPLINKING_HANDLER_TO_ROUTE_MAP.keySet()) {
            MobileDeepLinking.registerHandler(handler, new org.mobiledeeplinking.android.Handler() {
                @Override
                public Map<String, String> execute(Map<String, String> routeParameters) {
                    String route = MainActivity.DEEPLINKING_HANDLER_TO_ROUTE_MAP.get(handler);
                    Intent i = MainActivity.newIntent(contextForIntent, route);
                    Bundle extras = i.getExtras();
                    for (String key : extras.keySet()) {
                        routeParameters.put(key, extras.getString(key));
                    }
                    return routeParameters;

                }
            });
        }
    }

    public static Intent newIntent(Context packageContext, String route) {
        Intent i = new Intent(packageContext, MainActivity.class);
        i.putExtra(EXTRA_DEEP_LINKING_ROUTE, route);
        return i;
    }

    private static String deepLinkingRoute(Intent route) {
        return route.getStringExtra(EXTRA_DEEP_LINKING_ROUTE);
    }

    public void selectNavigationItem(int menuId) {
        Menu menu = navigationView.getMenu();
        for (int i = 0; i < menu.size(); i++) {
            if (menu.getItem(i).getItemId() == menuId) {
                selectNavigationItem(menu.getItem(i));
                break;
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        selectNavigationItem(item);
        return true;
    }

    @Override
    public void onLoginStateChanged(boolean isLoggedIn) {
        updateLoginState();
    }

    private void selectNavigationItem(MenuItem item) {
        navigationView.setCheckedItem(item.getItemId());

        // Find the current menu position.
        Menu menu = navigationView.getMenu();
        for (int i = 0; i < menu.size(); i++) {
            if (menu.getItem(i) == item && item.getGroupId() == R.id.navGroup) {
                getSupportActionBar().setTitle(item.getTitle());
                break;
            }
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment;
        String tag = item.getItemId() + "";

        switch (item.getItemId()) {
            case R.id.nav_home:
            case R.id.nav_deals:
            case R.id.nav_listings:
            case R.id.nav_events:
            case R.id.nav_my_area:
                fragment = fragmentManager.findFragmentByTag(tag);
                if (fragment == null) {
                    fragment = newFragment(item.getItemId());
                    fragmentManager.beginTransaction().replace(R.id.container, fragment, tag).commit();
                }
                break;
            case R.id.nav_login:
                if (fragmentManager.findFragmentByTag(tag) == null) {
                    new LoginSignUpDialogFragment().show(fragmentManager, tag);
                }
                break;

            case R.id.nav_logout:
                new AlertDialog.Builder(this)
                    .setTitle("Logout of ZoomLocal")
                    .setMessage("Are you sure you want to logout of ZoomLocal")
                    .setNegativeButton("Cancel", null)
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (userData.isLoggedIn()) {
                                sendLogoutSilently();
                                userData.logout();
                            }
                        }
                    })
                    .show();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    private Fragment newFragment(int menuId) {
        switch (menuId) {
            case R.id.nav_home:
                return new HomeFragment();
            case R.id.nav_deals:
                return new CouponsFragment();
            case R.id.nav_listings:
                return ProductsFragment.newInstance(ProductType.LISTING);
            case R.id.nav_events:
                return ProductsFragment.newInstance(ProductType.EVENT);
            case R.id.nav_my_area:
                return new VendorsOnMapFragment();
            default:
                throw new IllegalArgumentException("Unknown menu: " + menuId);
        }
    }

    private void sendLogoutSilently() {
        Call<ResponseBody> logoutCall = userService.logout(userData.getAccessToken(), userData.getRefreshToken());
        logoutCall.enqueue(new SilentCallback<ResponseBody>());
    }

    private void updateLoginState() {
        navigationView.getMenu().findItem(R.id.nav_login).setVisible(!userData.isLoggedIn());
        navigationView.getMenu().findItem(R.id.nav_logout).setVisible(userData.isLoggedIn());
    }

    private void startAdAfterSplashActivityIfNeeded() {
        AdAfterSplashDownloader adAfterSplashDownloader = App.getInstance().getAdAfterSplashDownloader();
        if (adAfterSplashDownloader.getAdAfterSplashBitmap() != null) {
            Intent intent = new Intent(this, AdAfterSplashActivity.class);
            startActivity(intent);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

}
