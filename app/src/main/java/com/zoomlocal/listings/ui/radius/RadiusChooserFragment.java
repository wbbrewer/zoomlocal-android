package com.zoomlocal.listings.ui.radius;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.zoomlocal.listings.App;
import com.zoomlocal.listings.LocalDataManager;
import com.zoomlocal.listings.R;
import com.zoomlocal.listings.util.Util;

/**
 */
public class RadiusChooserFragment extends Fragment implements OnMapReadyCallback, AdapterView.OnItemSelectedListener {
    private LocalDataManager localDataManager = App.getInstance().getLocalDataManager();

    private GoogleMap googleMap;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_radius_chooser, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.googleMap);
        mapFragment.getMapAsync(this);

        Spinner radiusSpinner = (Spinner) view.findViewById(R.id.radiusSpinner);
        radiusSpinner.setSelection(localDataManager.getRadius().ordinal());
        radiusSpinner.setOnItemSelectedListener(this);

        return view;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        updateRadius();
    }

    private void updateRadius() {
        if (googleMap != null) {
            LatLng latLng = Util.toLatLng(localDataManager.getLocation());

            googleMap.clear();

            googleMap.addMarker(new MarkerOptions()
                    .position(latLng)
                    .title("Current Position")
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA))
            );

            Circle circle = googleMap.addCircle(new CircleOptions()
                    .center(latLng)
                    .fillColor(Color.argb(55, 255, 0, 0))
                    .strokeWidth(0)
                    .radius(Util.milesToMeters(localDataManager.getRadiusMiles()))
            );

            // Zoom in, animating the camera.
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, getZoomLevel(circle)), 1000, null);
        }
    }

    public int getZoomLevel(Circle circle) {
        double radius = circle.getRadius();
        double scale = radius / 500;
        return (int) (15 - Math.log(scale) / Math.log(2));
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
        localDataManager.setRadius(LocalDataManager.Radius.values()[position]);

        updateRadius();
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
