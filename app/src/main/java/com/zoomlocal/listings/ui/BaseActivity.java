package com.zoomlocal.listings.ui;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.zoomlocal.listings.util.Util;

import io.nlopez.smartlocation.SmartLocation;

/**
 *
 */

public abstract class BaseActivity extends AppCompatActivity {
    private static final int LOCATION_REQUEST_CODE = 123;
    private static final String TAG = "BaseActivity";

    private CallbackManager callbackManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && !Util.locationPermissionGranted(this)) {
            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
        }

        callbackManager = CallbackManager.Factory.create();
    }

    public CallbackManager getCallbackManager() {
        return callbackManager;
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode) {
        try {
            super.startActivityForResult(intent, requestCode);
        } catch (NullPointerException | ActivityNotFoundException e) {
            Log.d(TAG, "Cannot startActivityForResult. Probably because of the bug in google play services ", e);
            Toast.makeText(this, "Cannot open the screen. Check your device configuration.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode, @Nullable Bundle options) {
        try {
            // to handle situation when google play services are not available but SupportMapFragment tries to open google play which is also not available
            // android.content.ActivityNotFoundException: No Activity found to handle Intent { act=android.intent.action.VIEW dat=market://details?id=com.google.android.gms&pcampaignid=gcore_9452000--- flg=0x80000 pkg=com.android.vending }
            super.startActivityForResult(intent, requestCode, options);
        } catch (NullPointerException | ActivityNotFoundException e) {
            Log.d(TAG, "Cannot startActivityForResult. Probably because of the bug in google play services ", e);
            Toast.makeText(this, "Cannot open the screen. Check your device configuration.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay!
                    Location lastLocation = SmartLocation.with(this).location().getLastLocation();
                    Log.d(TAG, "lastLocation: " + (lastLocation == null ? "unknown" :
                        String.format("LatLng: %s, %s", lastLocation.getLatitude(), lastLocation.getLongitude())));
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
            }
        }
    }
}
