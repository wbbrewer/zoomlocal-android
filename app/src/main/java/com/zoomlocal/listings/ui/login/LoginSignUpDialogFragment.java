package com.zoomlocal.listings.ui.login;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.zoomlocal.listings.R;
import com.zoomlocal.listings.databinding.FragmentLoginBinding;
import com.zoomlocal.listings.databinding.ViewLoginBinding;
import com.zoomlocal.listings.databinding.ViewResetPasswordBinding;
import com.zoomlocal.listings.databinding.ViewSignupBinding;

/**
 *
 */

public class LoginSignUpDialogFragment extends DialogFragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);

        View view = inflater.inflate(R.layout.fragment_login, container, false);

        FragmentLoginBinding fragmentLoginBinding = DataBindingUtil.bind(view);
        LoginSignUpViewModel loginSignUpViewModel = new LoginSignUpViewModel(this);
        fragmentLoginBinding.setViewModel(loginSignUpViewModel);

        ViewLoginBinding viewLoginBinding = DataBindingUtil.bind(view.findViewById(R.id.loginView));
        LoginViewModel loginViewModel = new LoginViewModel(loginSignUpViewModel);
        viewLoginBinding.setViewModel(loginViewModel);

        ViewSignupBinding viewSignupBinding = DataBindingUtil.bind(view.findViewById(R.id.signUpView));
        SignUpViewModel signUpViewModel = new SignUpViewModel(loginSignUpViewModel);
        viewSignupBinding.setViewModel(signUpViewModel);

        ViewResetPasswordBinding viewResetPasswordBinding = DataBindingUtil.bind(view.findViewById(R.id.resetView));
        ResetViewModel resetViewModel = new ResetViewModel(loginSignUpViewModel);
        viewResetPasswordBinding.setViewModel(resetViewModel);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        int width = (int) (getResources().getDisplayMetrics().widthPixels * 0.90);
        int height = (int) (getResources().getDisplayMetrics().heightPixels * 0.90);

        getDialog().getWindow().setLayout(width, height);
    }
}
