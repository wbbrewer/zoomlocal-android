package com.zoomlocal.listings.ui.home;

/**
 *
 */

public class HomeItem {
    private String name;

    private int icon;

    public HomeItem(String name, int icon) {
        this.name = name;
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }
}
