package com.zoomlocal.listings.ui.login;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.zoomlocal.listings.App;
import com.zoomlocal.listings.BR;
import com.zoomlocal.listings.R;
import com.zoomlocal.listings.model.LoginInfo;
import com.zoomlocal.listings.model.SignUpError;
import com.zoomlocal.listings.rest.ProgressedCallback;
import com.zoomlocal.listings.rest.UserService;
import com.zoomlocal.listings.util.SimpleTextWatcher;
import com.zoomlocal.listings.util.Util;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by aricbrown on 11/5/15.
 */
public class SignUpViewModel extends BaseObservable {
    public static final String TAG = SignUpViewModel.class.getSimpleName();

    private LoginSignUpViewModel loginSignUpViewModel;

    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String zipCode;

    public SignUpViewModel(LoginSignUpViewModel loginSignUpViewModel) {
        this.loginSignUpViewModel = loginSignUpViewModel;
    }

    //todo: use Observable map to display errors https://developer.android.com/topic/libraries/data-binding/index.html#observable_collections
    @Bindable
    public String getFirstNameError() {
        if (TextUtils.isEmpty(firstName)) {
            return App.getResourcesStatic().getString(R.string.error_first_name);
        }
        return null;
    }

    @Bindable
    public String getLastNameError() {
        if (TextUtils.isEmpty(lastName)) {
            return App.getResourcesStatic().getString(R.string.error_last_name);
        }
        return null;
    }

    @Bindable
    public String getEmailError() {
        if (TextUtils.isEmpty(email)) {
            return App.getResourcesStatic().getString(R.string.error_email);
        } else if (!Util.isEmailValid(email)) {
            return App.getResourcesStatic().getString(R.string.error_email_valid);
        }
        return null;
    }

    @Bindable
    public String getZipError() {
        if (TextUtils.isEmpty(zipCode)) {
            return App.getResourcesStatic().getString(R.string.error_zip_code);
        } else if (zipCode.length() != 5) {
            return "Enter 5 digits for zip code";
        }
        return null;
    }

    @Bindable
    public String getPasswordError() {
        if (TextUtils.isEmpty(password)) {
            return App.getResourcesStatic().getString(R.string.error_password);
        } else if (password.length() < 6) {
            return "Password should have at least 6 symbols";
        }
        return null;
    }

    public TextWatcher getValidationWatcher() {
        return new SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                notifyPropertyChanged(BR.firstNameError);
                notifyPropertyChanged(BR.lastNameError);
                notifyPropertyChanged(BR.emailError);
                notifyPropertyChanged(BR.passwordError);
                notifyPropertyChanged(BR.zipError);
            }
        };
    }

    public void onClickCreateAccount(final View view) {
        if (getFirstNameError() == null && getLastNameError() == null && getEmailError() == null) {
            Call<ResponseBody> call = App.getInstance().getUserService().signUp(firstName, lastName, email, zipCode, password);
            call.enqueue(new ProgressedCallback<ResponseBody>(view.getContext(), loginSignUpViewModel) {
                @Override
                protected void onSuccess(Call<ResponseBody> call, Response<ResponseBody> response) {
                    showToast("Sign up succeeded");

                    Call<LoginInfo> loginCall = App.getInstance().getUserService().login("password",
                        UserService.CLIENT_ID, UserService.CLIENT_SECRET, email, password);
                    loginCall.enqueue(new LoginCallbackHandler(view.getContext(), loginSignUpViewModel, email));
                }

                @Override
                protected void onServerError(Call<ResponseBody> call, Response<ResponseBody> response) {
                    try {
                        SignUpError error = App.getInstance().getGson().fromJson(response.errorBody().string(), SignUpError.class);
                        showToast(error.getErrorDescription());
                    } catch (Exception e) {
                        Log.d("test", "Error during parsing error", e);
                        super.onServerError(call, response);
                    }
                }
            });
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
