package com.zoomlocal.listings.ui.login;

import android.content.Context;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.zoomlocal.listings.App;
import com.zoomlocal.listings.BuildConfig;
import com.zoomlocal.listings.model.LoginError;
import com.zoomlocal.listings.model.LoginInfo;
import com.zoomlocal.listings.rest.ProgressedCallback;

import retrofit2.Call;
import retrofit2.Response;

/**
 */
public class LoginCallbackHandler extends ProgressedCallback<LoginInfo> {

    private final LoginSignUpViewModel loginSignUpViewModel;
    private final String email;

    public LoginCallbackHandler(Context context, LoginSignUpViewModel loginSignUpViewModel, String email) {
        super(context, loginSignUpViewModel);
        this.loginSignUpViewModel = loginSignUpViewModel;
        this.email = email;
    }

    @Override
    protected void onSuccess(Call<LoginInfo> call, Response<LoginInfo> response) {
        loginSignUpViewModel.close();

        App.getInstance().getUserData().login(response.body());

        if (!BuildConfig.DEBUG) {
            Crashlytics.setUserEmail(email);
        }
    }

    @Override
    protected void onServerError(Call<LoginInfo> call, Response<LoginInfo> response) {
        try {
            LoginError loginError = App.getInstance().getGson().fromJson(response.errorBody().string(), LoginError.class);
            showToast(loginError.getErrorDescription());
        } catch (Exception e) {
            Log.d("test", "Error during parsing login error", e);
            super.onServerError(call, response);
        }
    }
}
