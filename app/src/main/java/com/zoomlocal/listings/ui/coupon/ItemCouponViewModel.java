package com.zoomlocal.listings.ui.coupon;

import com.zoomlocal.listings.model.Coupon;

/**
 *
 */

public class ItemCouponViewModel {
    private Coupon coupon;

    public ItemCouponViewModel(Coupon coupon) {
        this.coupon = coupon;
    }

    public Coupon getCoupon() {
        return coupon;
    }

    public String getTitle() {
        return coupon.getTitle();
    }

    public String getVendorName() {
        return coupon.getTerms().getVendor().getVendorMeta().getName();
    }

    public String getImage() {
        return coupon.getImages().getThumbnail().getUrl();
    }

    public String getVendorAddress() {
        return coupon.getTerms().getVendor().getCityState();
    }

    public String getRedeemInfo() {
        if (coupon.getRedeemingDate() == null) {
            return "Valid Thru " + coupon.getMeta().getCouponEndDateFormatted();
        } else {
            return "Redeemed";
        }
    }
}
