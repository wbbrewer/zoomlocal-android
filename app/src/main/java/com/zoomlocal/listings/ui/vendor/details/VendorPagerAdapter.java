package com.zoomlocal.listings.ui.vendor.details;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.zoomlocal.listings.R;
import com.zoomlocal.listings.model.Vendor;
import com.zoomlocal.listings.ui.coupon.CouponsFragment;
import com.zoomlocal.listings.ui.listing.ProductsFragment;
import com.zoomlocal.listings.util.ProductType;

/**
 * Created by aricbrown on 1/6/16.
 */
public class VendorPagerAdapter extends FragmentStatePagerAdapter {

    private Activity activity;
    private Vendor vendor;

    public VendorPagerAdapter(Activity activity, FragmentManager fm, Vendor vendor) {
        super(fm);
        this.activity = activity;
        this.vendor = vendor;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return VendorDetailTabFragment.newInstance(vendor);
            case 1:
                return ProductsFragment.newInstance(ProductType.ALL, vendor.getSlug(), false);
            case 2:
                return CouponsFragment.newInstance(vendor.getSlug());
            case 3:
                return VendorMapTabFragment.newInstance(vendor);
            default:
                throw new IllegalArgumentException("No page for position: " + position);
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return activity.getString(R.string.details);
            case 1:
                return activity.getString(R.string.items);
            case 2:
                return "Coupons";
            case 3:
                return activity.getString(R.string.map);
            default:
                throw new IllegalArgumentException("No page for position: " + position);
        }
    }
}
