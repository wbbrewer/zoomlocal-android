package com.zoomlocal.listings.ui.search;

import android.support.v7.widget.RecyclerView;

import com.zoomlocal.listings.model.Vendor;
import com.zoomlocal.listings.ui.PaginationAdapter;
import com.zoomlocal.listings.ui.vendor.list.VendorsAdapter;

import java.util.List;

import retrofit2.Call;

/**
 *
 */

public class VendorsSearchFragment extends BaseSearchFragment<Vendor> {


    @Override
    protected PaginationAdapter<Vendor, ? extends RecyclerView.ViewHolder> newPaginationAdapter() {
        return new VendorsAdapter();
    }

    @Override
    protected Call<List<Vendor>> newCall() {
        return apiService.searchVendor(getSearch());
    }
}
