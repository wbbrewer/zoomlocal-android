package com.zoomlocal.listings.ui;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 */
public abstract class PaginationAdapter<T, H extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<H> {
    private List<T> items = new ArrayList<>();

    @Override
    public abstract H onCreateViewHolder(ViewGroup parent, int viewType);

    @Override
    public abstract void onBindViewHolder(H holder, int position);

    @Override
    public int getItemCount() {
        return items.size();
    }

    public T getItem(int position) {
        return items.get(position);
    }

    public void setItems(List<T> newItems) {
        items = new ArrayList<>(newItems);
        notifyDataSetChanged();
    }

    public void addItems(List<T> newItems) {
        items.addAll(newItems);
        notifyItemRangeChanged(items.size() - newItems.size(), newItems.size());
    }
}
